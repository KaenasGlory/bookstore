﻿using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;

namespace BookStore.DataAccessLayer.Repositories
{
    public class PaymentRepository : BaseRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(ApplicationContext appContext) : base(appContext)
        {
        }
    }
}
