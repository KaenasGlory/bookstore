﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public class OrderItemRepository : BaseRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(ApplicationContext appContext) : base(appContext)
        { }

        public async Task<OrderPageModel> GetAllAsync(OrderFilterModel orderFilterModel)
        {
            var orderQuery = _appContext.OrderItems.Include(x => x.PrintingEdition).Include(x => x.Order).ThenInclude(x => x.User)
                .GroupBy(x => x.Order.Id)
                .Select(o => new OrderModel
                {
                    Id = o.Key,
                    CreationDate = o.Select(item => item.Order.CreationDate).FirstOrDefault(),
                    UserName = o.Select(item => item.Order.User.UserName).FirstOrDefault(),
                    Email = o.Select(item => item.Order.User.Email).FirstOrDefault(),
                    Items = o.Select(item => item
                    //{
                    //    PrintingEdition = item.PrintingEdition,
                    //    Amount = item.Amount
                    //}
                    //{   PrintingEdition = o.Select(i=>i.PrintingEdition).FirstOrDefault(),
                    //    Amount = o.Select(i => i.Amount).FirstOrDefault()}
                    ).ToList(),
                    TotalPrice = o.Select(i => i.Order.TotalPrice).FirstOrDefault(),
                    Status = o.Select(i => i.Order.Status).FirstOrDefault()
                });
            if (orderFilterModel.SortingOption == Entities.Enums.Enums.SortingOption.Asc)
            {
                orderQuery = orderQuery.OrderBy(o => orderFilterModel.SortingColumn);
            }
            if (orderFilterModel.SortingOption == Entities.Enums.Enums.SortingOption.Desc)
            {
                orderQuery = orderQuery.OrderByDescending(o => orderFilterModel.SortingColumn);
            }
            if (!(orderFilterModel.OrderStatus == Entities.Enums.Enums.OrderStatus.All))
            {
                orderQuery = orderQuery.Where(o => o.Status == orderFilterModel.OrderStatus);
            }
            if (orderFilterModel.UserId != 0)
            {
                orderQuery = orderQuery.Where(x => x.Id == orderFilterModel.UserId);
            }
            var itemsToSkip = (orderFilterModel.NumberOfPage - 1) * orderFilterModel.PageSize;
            var page = await (orderQuery).Skip(itemsToSkip).Take(orderFilterModel.PageSize).ToListAsync();

            var orderPageModel = new OrderPageModel()
            {
                Items = page,
                ItemsCount = orderQuery.Count()
            };
            return orderPageModel;
        }
    }
}
