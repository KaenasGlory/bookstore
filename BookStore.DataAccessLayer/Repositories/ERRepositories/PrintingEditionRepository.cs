﻿using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;


namespace BookStore.DataAccessLayer.Repositories
{
    public class PrintingEditionRepository : BaseRepository<PrintingEdition>, IPrintingEditionRepository
    {
        public PrintingEditionRepository(ApplicationContext appContext) : base(appContext)
        {
        }
    }
}
