﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ApplicationContext _appContext;

        public UserRepository(SignInManager<User> signInManager, UserManager<User> userManager, ApplicationContext appContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appContext = appContext;
        }

        public async Task<bool> UpdateAsync(User user)
        {
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
        }

        public async Task<bool> BlockByIdAsync(long id)
        {
            var user = await GetByIdAsync(id);
            if (user.LockoutEnd == null)
            {
                await _userManager.SetLockoutEndDateAsync(user, DateTime.MaxValue);
                return await UpdateAsync(user);
            }
            if (user.LockoutEnd != null)
            {
                user.LockoutEnd = null;
            }
            return await UpdateAsync(user);
        }

        public async Task<User> GetByIdAsync(long userID)
        {
            var user = await _appContext.Users.Where(x => x.Id == userID).FirstOrDefaultAsync();
            return user;
        }

        public async Task<User> GetByEmailAsync(string userEmail)
        {
            return await _userManager.FindByEmailAsync(userEmail);
        }

        public async Task<string> GetRoleAsync(User user)
        {
            return (await _userManager.GetRolesAsync(user)).FirstOrDefault();
        }

        public async Task<SignInResult> LogInAsync(User user, string password)
        {
            return await _signInManager.PasswordSignInAsync(user, password, false, false);
        }

        public async Task<string> RegisterAsync(User user, string password)
        {
            if ((await _userManager.FindByEmailAsync(user.Email)) != null)
            {
                return string.Empty;
            }
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                return string.Empty;
            }
            var roleResult = await _userManager.AddToRoleAsync(user, Enums.Role.User.ToString());
            if (result.Succeeded && roleResult.Succeeded)
            {
                return await _userManager.GenerateEmailConfirmationTokenAsync(user);
            }
            return string.Empty;
        }

        public async Task<bool> ConfirmEmailAsync(string email, string confirmationToken)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return false;
            }
            var result = await _userManager.ConfirmEmailAsync(user, confirmationToken);
            return result.Succeeded;
        }

        public async Task LogOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<bool> ForgotPasswordAsync(User user, string newPassword)
        {
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);
            return result.Succeeded;
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string currentPassword, string newPassword)
        {
            var result = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            return result;
        }

        public async Task<bool> EditProfileByAdminAsync(User userProfile)
        {
            var currentUserProfile = await GetByEmailAsync(userProfile.Email);
            if (currentUserProfile == null)
            {
                return false;
            }
            currentUserProfile.FirstName = userProfile.FirstName;
            currentUserProfile.LastName = userProfile.LastName;
            return await UpdateAsync(currentUserProfile);

        }

        public async Task<bool> DeleteByIdAsync(long id)
        {
            var user = await GetByIdAsync(id);
            if (user == null)
            {
                return false;
            }
            user.IsRemoved = true;
            return await UpdateAsync(user);
        }

        public async Task<UserModel> GetAllAsync(UserFilterModel filterModel)
        {
            var userRoles = _appContext.UserRoles.Where(ur => ur.RoleId ==
            (_appContext.Roles.Where(r => r.Name != Enums.Role.Admin.ToString()).Select(x => x.Id).FirstOrDefault()));
            var userQuery = _appContext.Users.Where(u => userRoles.Any(ur => ur.UserId == u.Id) && !u.IsRemoved);
            if (!string.IsNullOrEmpty(filterModel.SearchText))
            {
                userQuery = userQuery.Where(u => u.FirstName.Contains(filterModel.SearchText) || u.LastName.Contains(filterModel.SearchText) || u.Email.Contains(filterModel.SearchText));
            }
            if (filterModel.UserStatus == Enums.UserStatus.Active)
            {
                userQuery = userQuery.Where(u => u.LockoutEnd == null);
            }
            if (filterModel.UserStatus == Enums.UserStatus.Blocked)
            {
                userQuery = userQuery.Where(u => u.LockoutEnd != null);
            }
            var itemsToSkip = (filterModel.NumberOfPage - 1) * filterModel.PageSize;
            var page = await userQuery.Skip(itemsToSkip).Take(filterModel.PageSize).ToListAsync();
            var userModel = new UserModel
            {
                Items = page,
                ItemsCount = userQuery.Count()
            };
            return userModel;
        }
    }
}



