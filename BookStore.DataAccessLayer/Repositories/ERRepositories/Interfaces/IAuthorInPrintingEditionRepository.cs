﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using BookStore.DataAccessLayer.Models.Filters.Base;
using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IAuthorInPrintingEditionRepository : IBaseRepository<AuthorInPrintingEdition>
    {
        Task<bool> ReplaceAllAuthorsInPrintingEdition(long printingEditionId, List<long> authors);
        Task<AuthorPageModel> GetAllAuthorsAsync(BaseFilterModel filterModel);
        Task<PrintingEditionPageModel> GetAllPrintingEditionsAsync(PrintingEditionFilterModel filterModel);
        Task<List<long>> GetAuthorInPrintingEditionIdsByPrintingEditionIdAsync(long printingEditionId);
        Task<List<AuthorInPrintingEdition>> GetAuthorInPrintingEditionsByAuthorIdAsync(long authorId);
        Task<List<long>> GetPrintingEditionsWithoutAuthorAsync(List<long> printingEditionIds);
        Task<PrintingEditionModel> GetPrintingEditionModelByIdAsync(long id);
    }
}
