﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IBaseRepository<TEntity>
    {
        Task<long> CreateAsync(TEntity baseEntity);
        Task<bool> UpdateAsync(TEntity baseEntity);
        Task<bool> DeleteAsync(TEntity baseEntity);
        Task<bool> DeleteByIdAsync(long id);
        Task<TEntity> GetByIdAsync(long id);
        Task<List<TEntity>> GetAllAsync();
    }
}
