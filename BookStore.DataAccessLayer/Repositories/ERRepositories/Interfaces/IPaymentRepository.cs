﻿using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IPaymentRepository : IBaseRepository<Payment>
    {
    }
}
