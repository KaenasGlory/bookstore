﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
       
    }
}
