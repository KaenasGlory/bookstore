﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IUserRepository
    {
        Task<bool> UpdateAsync(User user);
        Task<bool> BlockByIdAsync(long userId);
        Task<User> GetByIdAsync(long userId);
        Task<User> GetByEmailAsync(string userEmail);
        Task<string> GetRoleAsync(User user);
        Task<SignInResult> LogInAsync(User user, string password);
        Task<string> RegisterAsync(User user, string password);
        Task<bool> ConfirmEmailAsync(string email, string confirmationToken);
        Task LogOutAsync();
        Task<bool> ForgotPasswordAsync(User user, string newPassword);
        Task<IdentityResult> ChangePasswordAsync(User user, string currentPassword, string newPassword);
        Task<bool> EditProfileByAdminAsync(User userProfile);
        Task<bool> DeleteByIdAsync(long id);
        Task<UserModel> GetAllAsync(UserFilterModel filterModel);

    }
}
