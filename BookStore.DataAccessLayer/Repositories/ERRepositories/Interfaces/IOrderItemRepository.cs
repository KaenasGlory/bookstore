﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IOrderItemRepository : IBaseRepository<OrderItem>
    {
        Task<OrderPageModel> GetAllAsync(OrderFilterModel orderFilterModel);
    }
}
