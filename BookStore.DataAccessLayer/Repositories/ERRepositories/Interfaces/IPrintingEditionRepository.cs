﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models.Filters;
using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IPrintingEditionRepository : IBaseRepository<PrintingEdition>
    {
    }
}
