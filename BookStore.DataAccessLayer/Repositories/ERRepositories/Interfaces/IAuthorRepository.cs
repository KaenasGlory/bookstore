﻿using DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public interface IAuthorRepository : IBaseRepository<Author>
    { 
    }
}
