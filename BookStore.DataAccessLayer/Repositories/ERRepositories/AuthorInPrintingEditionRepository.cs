﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Models.Filters;
using BookStore.DataAccessLayer.Models.Filters.Base;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public class AuthorInPrintingEditionRepository : BaseRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
    {
        public AuthorInPrintingEditionRepository(ApplicationContext appContext) : base(appContext)
        {
        }

        private decimal GetMaxPrice(IQueryable<PrintingEditionModel> queryable)
        {
            var printingEditionWithAuthorsModel = queryable.OrderByDescending(p => p.Price).First();
            return printingEditionWithAuthorsModel.Price;
        }

        private decimal GetMinPrice(IQueryable<PrintingEditionModel> queryable)
        {
            var printingEditionWithAuthorsModel = queryable.OrderByDescending(p => p.Price).Last();
            return printingEditionWithAuthorsModel.Price;
        }

        public async Task<bool> ReplaceAllAuthorsInPrintingEdition(long printingEditionId, List<long> authorIds)
        {
            var authorInPrintingEditions = _appContext.AuthorInPrintingEditions.Where(ap => ap.PrintingEditionId == printingEditionId && ap.IsRemoved == false);
            foreach (var authorId in authorIds)
            {
                var aleadyExist = authorInPrintingEditions.Where(x => x.AuthorId == authorId);
                if (aleadyExist.Any())
                {
                    continue;
                }
                var newAuthorInPrintingEdition = new AuthorInPrintingEdition
                {
                    PrintingEditionId = printingEditionId,
                    AuthorId = authorId
                };
                await CreateAsync(newAuthorInPrintingEdition);

            }
            foreach (var authorInPrintingEdition in authorInPrintingEditions)
            {
                if (!authorIds.Contains(authorInPrintingEdition.AuthorId))
                {
                    var oldAuthorInPrintingEdition = await authorInPrintingEditions.Where(x => x.AuthorId == authorInPrintingEdition.AuthorId).FirstOrDefaultAsync();
                    _appContext.AuthorInPrintingEditions.Remove(oldAuthorInPrintingEdition);
                }
            }
            return true;
        }

        public async Task<PrintingEditionPageModel> GetAllPrintingEditionsAsync(PrintingEditionFilterModel filterModel)
        {
            var printingEditionQuery = _appContext.AuthorInPrintingEditions.Include(x => x.PrintingEdition).Include(x => x.Author).Where(ap => !ap.PrintingEdition.IsRemoved)
                .GroupBy(x => x.PrintingEditionId)
                .Select(group => new PrintingEditionModel
                {
                    Id = group.Key,
                    Title = group.Select(ap => ap.PrintingEdition.Title).FirstOrDefault(),
                    Description = group.Select(ap => ap.PrintingEdition.Description).FirstOrDefault(),
                    Type = group.Where(ap => ap.Author.IsRemoved == false).Select(ap => ap.PrintingEdition.PrintingEditionType).FirstOrDefault(),
                    Authors = group.Select(a => a.Author).ToList(),
                    Price = group.Select(ap => ap.PrintingEdition.Price).FirstOrDefault()
                });

            if (!string.IsNullOrEmpty(filterModel.SearchText))
            {
                printingEditionQuery = printingEditionQuery.Where(x => x.Title.Contains(filterModel.SearchText)
                || x.Authors.Any(a => a.FirstName.Contains(filterModel.SearchText) || a.LastName.Contains(filterModel.SearchText)));
            }

            if (filterModel.MinPrice == 0)
            {
                filterModel.MinPrice = GetMinPrice(printingEditionQuery);
            }
            if (filterModel.MaxPrice == 0)
            {
                filterModel.MaxPrice = GetMaxPrice(printingEditionQuery);
            }

            printingEditionQuery = printingEditionQuery.Where(x => x.Price >= filterModel.MinPrice && x.Price <= filterModel.MaxPrice).AsQueryable();

            foreach (var type in (Enums.PrintingEditionType[])Enum.GetValues(typeof(Enums.PrintingEditionType)))
            {
                if (!filterModel.SelectedPrintingEditionTypes.Contains(type))
                {
                    printingEditionQuery = printingEditionQuery.Where(p => p.Type != type);
                }
            }
            if (filterModel.SortingOption == Enums.SortingOption.Asc)
            {
                printingEditionQuery = printingEditionQuery.OrderBy(p => filterModel.SortingColumn);
            }
            if (filterModel.SortingOption == Enums.SortingOption.Desc)
            {
                printingEditionQuery = printingEditionQuery.OrderByDescending(p => filterModel.SortingColumn);
            }
            var itemsToSkip = (filterModel.NumberOfPage - 1) * filterModel.PageSize;
            var page = await (printingEditionQuery).Skip(itemsToSkip).Take(filterModel.PageSize).ToListAsync();
            var result = new PrintingEditionPageModel
            {
                Items = page,
                ItemsCount = printingEditionQuery.Count(),
                MinPrice = filterModel.MinPrice,
                MaxPrice = filterModel.MaxPrice
            };
            return result;
        }

        public async Task<AuthorPageModel> GetAllAuthorsAsync(BaseFilterModel filterModel)
        {
            var authorQuery = _appContext.Authors.Where(x => x.IsRemoved == false)
              .Select(a => new AuthorWithPrintingEditionsModel
              {
                  Id = a.Id,
                  FirstName = a.FirstName,
                  LastName = a.LastName,
                  PrintingEditions = _appContext.AuthorInPrintingEditions.Where(x => x.AuthorId == a.Id).Select(x => x.PrintingEdition.Title).ToList()
              });

            if (!string.IsNullOrWhiteSpace(filterModel.SearchText))
            {
                authorQuery = authorQuery.Where(x => x.FirstName.Contains(filterModel.SearchText) || x.LastName.Contains(filterModel.SearchText));
            }

            if (filterModel.SortingOption == Enums.SortingOption.Asc)
            {
                authorQuery = authorQuery.OrderBy(p => p.Id);
            }
            if (filterModel.SortingOption == Enums.SortingOption.Desc)
            {
                authorQuery = authorQuery.OrderByDescending(p => p.Id);
            }

            var itemsToSkip = (filterModel.NumberOfPage - 1) * filterModel.PageSize;
            var page = await (authorQuery).Skip(itemsToSkip).Take(filterModel.PageSize).ToListAsync();
            var result = new AuthorPageModel
            {
                Items = page,
                ItemsCount = authorQuery.Count()
            };
            return result;
            //var authorQuery = _appContext.AuthorInPrintingEditions.Include(x => x.PrintingEdition).Include(x => x.Author).Where(ap => ap.Author.IsRemoved == false)
            //    .GroupBy(x => x.AuthorId)
            //    .Select(group => new AuthorWithPrintingEditionsModel
            //    {
            //        Id = group.Key,
            //        Name = group.Select(a =>a.Author.FirstName).FirstOrDefault()+" "+ group.Select(a => a.Author.LastName).FirstOrDefault(),
            //        PrintingEditions = group.Where(a => a.PrintingEdition.IsRemoved == false).Select(a => a.PrintingEdition.Title).ToList()
            //    });

            //if (filterModel.SortingOption == Enums.SortingOption.Asc)
            //{
            //    authorQuery = authorQuery.OrderBy(p => filterModel.SortingColumn);
            //}
            //if (filterModel.SortingOption == Enums.SortingOption.Desc)
            //{
            //    authorQuery = authorQuery.OrderByDescending(p => filterModel.SortingColumn);
            //}

            //var itemsToSkip = (filterModel.NumberOfPage - 1) * filterModel.PageSize;
            //var page = await (authorQuery).Skip(itemsToSkip).Take(filterModel.PageSize).ToListAsync();
            //var result = new AuthorPageModel
            //{
            //    Items = page,
            //    CountItems = authorQuery.Count()
            //};
            //return result;
        }

        public async Task<List<long>> GetAuthorInPrintingEditionIdsByPrintingEditionIdAsync(long printingEditionId)
        {
            var authorInPrintingEditionIds = await _appContext.AuthorInPrintingEditions.Where(x => x.PrintingEditionId == printingEditionId)
                .Select(l => l.Id)
                .ToListAsync();
            return authorInPrintingEditionIds;
        }

        public async Task<List<AuthorInPrintingEdition>> GetAuthorInPrintingEditionsByAuthorIdAsync(long authorId)
        {
            var authorInPrintingEditions = await _appContext.AuthorInPrintingEditions.Where(x => x.AuthorId == authorId).ToListAsync();
            return authorInPrintingEditions;
        }

        public async Task<List<long>> GetPrintingEditionsWithoutAuthorAsync(List<long> printingEditionIds)
        {
            var deletablePrintingEditionIds = await _appContext.AuthorInPrintingEditions.Where(x => !x.PrintingEditionId.Equals(printingEditionIds.Any())).Select(x => x.PrintingEditionId).ToListAsync();
            return deletablePrintingEditionIds;
        }

        public async Task<PrintingEditionModel> GetPrintingEditionModelByIdAsync(long id)
        {
            var printingEditionModel = _appContext.AuthorInPrintingEditions.Include(ap => ap.PrintingEdition)
                .Include(ap => ap.Author)
                .Where(ap => ap.PrintingEditionId == id && !ap.IsRemoved)
                .GroupBy(ap => ap.PrintingEditionId)
                .Select((group) => new PrintingEditionModel
                {
                    Id = group.Select(p => p.PrintingEditionId).FirstOrDefault(),
                    Title = group.Select(p => p.PrintingEdition.Title).FirstOrDefault(),
                    Description = group.Select(p => p.PrintingEdition.Description).FirstOrDefault(),
                    Price = group.Select(p => p.PrintingEdition.Price).FirstOrDefault(),
                    Type = group.Select(p => p.PrintingEdition.PrintingEditionType).FirstOrDefault(),
                    Authors = group.Select(p => p.Author).ToList()
                });
            return await printingEditionModel.FirstOrDefaultAsync();
        }
    }
}
