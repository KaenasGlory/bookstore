﻿using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;

namespace BookStore.DataAccessLayer.Repositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationContext appContext) : base(appContext)
        {
        }
    }
}
