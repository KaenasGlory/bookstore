﻿using BookStore.DataAccessLayer.Entities.Base;
using DataAccessLayer.AppContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationContext _appContext;
        public BaseRepository(ApplicationContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<long> CreateAsync(TEntity baseEntity)
        {
            baseEntity.CreationDate = DateTime.Now;
            var result = await _appContext.AddAsync(baseEntity);
            await _appContext.SaveChangesAsync();
            return result.Entity.Id;
        }

        public async Task<bool> DeleteAsync(TEntity baseEntity)
        {
            baseEntity.IsRemoved = true;
            var result = await _appContext.SaveChangesAsync();
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteByIdAsync(long id)
        {
            var entity = await GetByIdAsync(id);
            entity.IsRemoved = true;
            var result = await UpdateAsync(entity);
            return result;
        }

        public Task<List<TEntity>> GetAllAsync()
        {
            return _appContext.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            var entity = await _appContext.Set<TEntity>()
                 .AsNoTracking()
                 .FirstOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        public async Task<bool> UpdateAsync(TEntity baseEntity)
        {

            _appContext.Update(baseEntity);
            var result = await _appContext.SaveChangesAsync();
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}
