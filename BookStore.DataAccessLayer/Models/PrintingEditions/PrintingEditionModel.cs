﻿using BookStore.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models
{
    public class PrintingEditionModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Enums.PrintingEditionType Type { get; set; }
        public List<Author> Authors { get; set; } = new List<Author>();
        public decimal Price { get; set; }
    }
}
