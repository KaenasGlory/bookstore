﻿using BookStore.DataAccessLayer.Models.Base;

namespace BookStore.DataAccessLayer.Models
{
    public class PrintingEditionPageModel : BasePageModel<PrintingEditionModel>
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}
