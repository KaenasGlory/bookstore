﻿using BookStore.DataAccessLayer.Entities.Enums;

namespace BookStore.DataAccessLayer.Models
{
    public class OrderModelItem
    {
        public string Title { get; set; }
        public int Amount { get; set; }
        public Enums.PrintingEditionType Type { get; set; }
    }
}
