﻿using BookStore.DataAccessLayer.Entities.Enums;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models
{
    public class OrderModel
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<OrderItem> Items { get; set; } = new List<OrderItem>();
        public decimal TotalPrice { get; set; }
        public Enums.OrderStatus Status { get; set; }
    }
}
