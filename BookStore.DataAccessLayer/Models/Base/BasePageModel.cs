﻿using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models.Base
{
    public class BasePageModel<T>
    {
        public List<T> Items { get; set; } = new List<T>();
        public int ItemsCount { get; set; }
    }
}
