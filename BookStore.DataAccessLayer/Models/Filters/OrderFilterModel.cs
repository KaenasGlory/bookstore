﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models.Filters.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccessLayer.Models.Filters
{
    public class OrderFilterModel : BaseFilterModel
    {
        public Enums.OrderStatus OrderStatus { get; set; }
        public long UserId { get; set; }
    }
}
