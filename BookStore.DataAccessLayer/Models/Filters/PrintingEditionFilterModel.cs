﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models.Filters.Base;
using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models.Filters

{
    public class PrintingEditionFilterModel : BaseFilterModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public List<Enums.PrintingEditionType> SelectedPrintingEditionTypes { get; set; }

    }
}
