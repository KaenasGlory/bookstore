﻿using BookStore.DataAccessLayer.Entities.Enums;

namespace BookStore.DataAccessLayer.Models.Filters.Base
{
    public class BaseFilterModel
    {
        public int NumberOfPage { get; set; } 
        public int PageSize { get; set; }
        public string SearchText { get; set; }
        public Enums.SortingOption SortingOption { get; set; }
        public Enums.SortingColumn SortingColumn { get; set; }
    }
}
