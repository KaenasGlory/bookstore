﻿using BookStore.DataAccessLayer.Entities.Enums;
using BookStore.DataAccessLayer.Models.Filters.Base;
using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models.Filters
{
    public class UserFilterModel : BaseFilterModel
    {
        public Enums.UserStatus UserStatus { get; set; }
    }
}
