﻿using System.Collections.Generic;

namespace BookStore.DataAccessLayer.Models
{
    public class AuthorWithPrintingEditionsModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> PrintingEditions { get; set; } = new List<string>();
    }
}
