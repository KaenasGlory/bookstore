﻿using BookStore.DataAccessLayer.Constants;
using BookStore.DataAccessLayer.Entities.Enums;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;

namespace BookStore.DataAccessLayer.Initialization
{
    public class DbInizialization
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole<long>> _roleManager;
        private readonly ApplicationContext _applicationContext;
        public DbInizialization(ApplicationContext applicationContext, UserManager<User> userManager, RoleManager<IdentityRole<long>> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _applicationContext = applicationContext;
        }
        public void Initialize()
        {
            using (var transaction = _applicationContext.Database.BeginTransaction())
            {
                var roles = _roleManager.Roles.ToList();
                try
                {
                    if (roles == null || !roles.Any())
                    {
                        var admin = new IdentityRole<long>()
                        {
                            Name = Enums.Role.Admin.ToString()
                        };
                        _roleManager.CreateAsync(admin).GetAwaiter().GetResult();

                        var user = new IdentityRole<long>()
                        {
                            Name = Enums.Role.User.ToString()
                        };
                        _roleManager.CreateAsync(user).GetAwaiter().GetResult();
                    }
                    var foundedUser = _userManager.FindByEmailAsync(DBInitializationParameters.AdminEmail).GetAwaiter().GetResult();
                    if (foundedUser == null)
                    {
                        var admin = new User
                        {
                            FirstName = DBInitializationParameters.AdminFirstName,
                            Email = DBInitializationParameters.AdminEmail,
                            UserName = DBInitializationParameters.AdminEmail,
                            EmailConfirmed = DBInitializationParameters.AdminEmailConfirmed
                        };
                        _userManager.CreateAsync(admin, DBInitializationParameters.AdminPassword).GetAwaiter().GetResult();
                        _userManager.AddToRoleAsync(admin, Enums.Role.Admin.ToString()).GetAwaiter().GetResult();

                        var user = new User
                        {
                            FirstName = DBInitializationParameters.UserFirstName,
                            Email = DBInitializationParameters.UserEmail,
                            UserName = DBInitializationParameters.UserEmail,
                            EmailConfirmed = DBInitializationParameters.UserEmailConfirmed
                        };
                        _userManager.CreateAsync(user).GetAwaiter().GetResult();
                        _userManager.AddToRoleAsync(user, Enums.Role.User.ToString()).GetAwaiter().GetResult();

                        var author = new Author 
                        { 
                            FirstName = DBInitializationParameters.AuthorFirstName, 
                            LastName = DBInitializationParameters.AuthorLastName
                        };
                        _applicationContext.AddAsync(author).GetAwaiter().GetResult();

                        var printingEdition = new PrintingEdition
                        {
                            Title = DBInitializationParameters.PrintingEditionTitle,
                            Description = DBInitializationParameters.PrintingEditionDescription,
                            Price = DBInitializationParameters.PrintingEditionPrice,
                            Currency = Enums.Currency.EUR,
                            PrintingEditionType = Enums.PrintingEditionType.Book
                        };
                        _applicationContext.AddAsync(printingEdition).GetAwaiter().GetResult();

                        var authorInPrintingEdition = new AuthorInPrintingEdition
                        {
                            AuthorId = author.Id,
                            PrintingEditionId = printingEdition.Id,
                            Author = author,
                            PrintingEdition = printingEdition
                        };
                        _applicationContext.AddAsync(authorInPrintingEdition).GetAwaiter().GetResult();
                        _applicationContext.SaveChanges();
                        transaction.Commit();
                    }
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    Console.WriteLine(exception);
                }
            }
        }
    }
}