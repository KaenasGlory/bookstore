﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum UserStatus
        {
            All = 0,
            Active = 1, 
            Blocked = 2
        }
    }
}
