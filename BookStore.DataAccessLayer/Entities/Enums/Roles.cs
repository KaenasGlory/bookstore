﻿namespace BookStore.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum Role
        {
            User = 0,
            Admin = 1
        }
    }
}