﻿namespace BookStore.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum PrintingEditionType
        {
            Book = 0,
            Newspaper = 1,
            Magazine = 2,
        }
    }
}