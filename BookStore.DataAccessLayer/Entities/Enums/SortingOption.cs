﻿namespace BookStore.DataAccessLayer.Entities.Enums
{
    public partial class Enums
    {
        public enum SortingOption
        {
            None = 0,
            Asc = 1,
            Desc = 2
        }
    }
}
