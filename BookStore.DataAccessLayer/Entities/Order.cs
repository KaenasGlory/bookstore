﻿using BookStore.DataAccessLayer.Entities.Base;
using BookStore.DataAccessLayer.Entities.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    public class Order : BaseEntity
    {
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        public long PaymentId { get; set; }
        [ForeignKey("PaymentId")]
        public Payment Payment { get; set; }
        public Enums.OrderStatus Status { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
