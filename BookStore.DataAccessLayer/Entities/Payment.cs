﻿using BookStore.DataAccessLayer.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    public class Payment : BaseEntity
    {
        public string TransactionId { get; set; }
    }
}
