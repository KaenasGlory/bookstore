﻿using BookStore.DataAccessLayer.Entities.Enums;

namespace BookStore.DataAccessLayer.Constants
{
    public class DBInitializationParameters
    {
        public static string AdminFirstName = "Oleksandra";
        public static string AdminEmail = "oleksandra.kuzmina@nure.ua";
        public static string AdminPassword = "Pass-123";
        public const bool AdminEmailConfirmed = true;
        public static string UserFirstName = "Ivan";
        public static string UserEmail = "c3332795@urhen.com";
        public const bool UserEmailConfirmed = true;
        public static string AuthorFirstName = "Lewis";
        public static string AuthorLastName = "Carroll";
        public static string PrintingEditionTitle = "Alice in Wonderland";
        public static string PrintingEditionDescription = "alice";
        public const decimal PrintingEditionPrice = 1.0m;
        public static string PrintingEditionCurrency = Enums.Currency.EUR.ToString();
        public static string PrintingEditionType = Enums.PrintingEditionType.Book.ToString();
    }
}
