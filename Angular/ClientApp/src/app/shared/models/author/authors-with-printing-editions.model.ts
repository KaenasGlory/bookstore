import { logging } from "protractor";
import { AuthorWithPrintingEditionsModel } from "./author-with-printing-editions.model";


export class AuthorsWithPrintingEditionsModel {
    items: Array<AuthorWithPrintingEditionsModel> = new Array<AuthorWithPrintingEditionsModel>();
}