import { logging } from "protractor";
import { AuthorModel } from "./author.model";


export class AuthorWithPrintingEditionsModel {
    author: AuthorModel = new AuthorModel();
    printingEditions: Array<string> = new Array<string>();
}