import { BaseModel } from "../base/base.model";
import { Roles } from "../../enums/roles";
import { UserStatus } from "../../enums/user-status";

export class UserModel extends BaseModel {
    id: number;
    firstName?: string;
    lastName?: string;
    email: string;
    status : UserStatus;
}