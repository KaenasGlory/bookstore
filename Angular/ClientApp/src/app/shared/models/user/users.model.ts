import { BaseModel } from "../base/base.model";
import { UserModel } from "./user.model";

export class UsersModel extends BaseModel {
    items: UserModel[];
    count: number;
}