import { BaseModel } from "../base/base.model";

export class UserChangePasswordModel{
    id: number;
    currentPassword:string;
    newPassword?:string;
}