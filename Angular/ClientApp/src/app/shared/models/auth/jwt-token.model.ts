export class JwtTokenModel {
    public accessToken : string;
    public refreshToken : string;
}