import { BaseModel } from "../base/base.model";

export class RegistrationModel {
    public firstName :string
    public lastName :string
    public email: string
    public password: string
}