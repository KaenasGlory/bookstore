import { BaseModel } from "../base/base.model";

export class LogInModel  {
    public email: string;
    public password: string;
}