import { BaseFilterModel } from "./base/base.filter.model";
import { UserStatus } from "../../enums/user-status";

export class UserFilterModel extends BaseFilterModel
{
    public userStatus : UserStatus
}