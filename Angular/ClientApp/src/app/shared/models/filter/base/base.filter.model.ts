import { SortingOption } from "src/app/shared/enums/sorting-option";
import { SortingColumn } from "src/app/shared/enums/sorting-column";
import { CommonConstants } from "src/app/shared/constants/common-constants";

export class BaseFilterModel
{
    numberOfPage : number;
    pageSize : number;
    searchText ?: string;
    sortingOption ?: SortingOption ;
    sortingColumn ?: SortingColumn;
}