import { PrintingEditionTypes } from "../../enums/printing-edition-types"

   export class OrderItemModelItem 
    {
        title : string;
        amount : number;
        type: PrintingEditionTypes;
    }