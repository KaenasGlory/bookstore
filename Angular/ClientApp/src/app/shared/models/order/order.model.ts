import { OrderItemModelItem } from "./order-item.model";

import { OrderStatus } from "../../enums/order-statuses";

export class OrderModel{
    id : number;
    date : Date;
    userName: string;
    email: string;
    items : Array<OrderItemModelItem> = new Array<OrderItemModelItem>();
    totalPrice : number
    status : OrderStatus
}