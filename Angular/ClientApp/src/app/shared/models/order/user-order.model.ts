import { OrderItemModelItem } from "./order-item.model";
import { OrderStatus } from "../../enums/order-statuses";

export class UserOrderModel
{
    id : number;
    date : Date;
    items : Array<OrderItemModelItem> = new Array<OrderItemModelItem>();
    totalPrice : number
    status : OrderStatus
}