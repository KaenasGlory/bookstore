import { PrintingEditionTypes } from "../../enums/printing-edition-types";
import { AuthorModel } from "../author/author.model";
import { Currencies } from "../../enums/currencies";
import { BaseModel } from "../base/base.model";

export class PrintingEditionModel extends BaseModel{
    id: number;
    title: string;
    description: string;
    type: PrintingEditionTypes;
    price: number;
    currency: Currencies;
    authors: Array<AuthorModel> = new Array<AuthorModel>();
}
