import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NG_VALIDATORS, Validator, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageCounter } from './services/page-counter';
import { CommonConstants } from './constants/common-constants';
import { RouterConstants } from './constants/router-constants';
import { RouterModule } from '@angular/router';
import { AccountService } from './services/account.service';
import { AdminService } from './services/admin.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Interceptor } from './interceptors/interceptor';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { LayoutComponent } from './components/layout/layout.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorService } from './services/error.service';
import { MaterialModule } from '../material/material.module';
import { ErrorConstants } from './constants/error-constants';
import { PasswordMatchValidator } from './services/password-match-validator';
import { CrossFieldErrorMatcher } from './services/cross-field-error-matcher';
import { TableHeaders } from './constants/table-headers';
import { ConfirmDeletingModalComponent} from './components/confirm-delete-modal/confirm-delete-modal.component';
import { LocalStorageService } from './services/local-storage.service';
import { UserService } from './services/user.service';
import { PrintingEditionService ,} from './services/index';

@NgModule({
  declarations: [
    NavMenuComponent,
    HomeComponent,
    LayoutComponent,
    ConfirmDeletingModalComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    MaterialModule,
    FormsModule
  ],
  providers: [
    AccountService,
    ErrorConstants,
    CommonConstants,
    AdminService,
    PageCounter,
    TableHeaders,
    ErrorService,
    CrossFieldErrorMatcher,
    LocalStorageService,
    PrintingEditionService,
    UserService,
    {
      provide: NG_VALIDATORS,
      useExisting: PasswordMatchValidator,
      multi: true
    },
     {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    } 
  ],
  exports: [
    NavMenuComponent,
    HomeComponent,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDeletingModalComponent

  ],
  entryComponents:[ConfirmDeletingModalComponent]
})
export class SharedModule { }
