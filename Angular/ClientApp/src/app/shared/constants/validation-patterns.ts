import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export  class ValidationPatterns {

  readonly name = "[A-Za-z]+";
  readonly password = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$";
  readonly email = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$";
}
