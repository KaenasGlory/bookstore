import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export  class RouterConstants {

    readonly serverLogin: string = environment.serverUrl + "api/account/login";
    readonly serverRegister: string = environment.serverUrl + "api/account/registration";
    readonly serverForgotPassword: string = environment.serverUrl + "api/account/forgotPassword";
    readonly serverConfirmAccount: string = environment.serverUrl + "api/account/confirmRegistration";
    readonly serverLogout: string = environment.serverUrl + "api/account/logout";
    readonly serverEditProfile: string = environment.serverUrl + "api/account/edit";
    readonly serverRefreshTokens: string = environment.serverUrl + "api/account/refresh";

    readonly serverGetAllUsers: string = environment.serverUrl + "api/user/getAll";
    readonly serverEditUser: string = environment.serverUrl + "api/user/edit";
    readonly serverChangePassword: string = environment.serverUrl + "api/user/changePassword";
    readonly serverDeleteUser: string = environment.serverUrl + "api/user/delete";
    readonly serverBlockUser: string = environment.serverUrl + "api/user/block";

    readonly serverCreateAuthor: string = environment.serverUrl + "api/author/create";
    readonly serverDeleteAuthor: string = environment.serverUrl + "api/author/delete";
    readonly serverEditAuthor: string = environment.serverUrl + "api/author/edit";
    readonly serverGetAllAuthors: string = environment.serverUrl + "api/author/getAll";

    readonly serverGetAllOrders: string = environment.serverUrl + "api/order/getAll";
    readonly serverAddOrder: string = environment.serverUrl + "api/order/all";

    readonly serverCreatePrintingEdition: string = environment.serverUrl + "api/printingEdition/create";
    readonly serverEditPrintingEdition: string = environment.serverUrl + "api/printingEdition/edit";
    readonly serverDeletePrintingEdition: string = environment.serverUrl + "api/printingEdition/delete";
    readonly serverGetAllPrintingEdition: string = environment.serverUrl + "api/printingEdition/getAll";
    readonly serverGetPrintingEdition: string = environment.serverUrl + "api/printingEdition/getPrintingEdition";


    readonly register: string = "/account/register";
    readonly login: string = "/account/login";
    readonly confirmAccount: string = environment.clientUrl +"account/confirm-account";
    readonly forgotPassword: string = "/account/forgot-password";
    readonly create: string = environment.clientUrl +"account/test";
}
