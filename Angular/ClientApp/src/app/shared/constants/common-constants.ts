import { Injectable } from '@angular/core';

@Injectable()
export class CommonConstants {
  public emptyString: string = '';
  public nullModel: string = 'No model arrived';
  public passwordLength: number = 6;
  public errorShowingDuration = 3000;
  public firstPage: number = 1
  public pageSizeOption: number[] = [6, 12, 24]
  public pageSize: number = 12;
  public defoultAmount:number = 1;
}
