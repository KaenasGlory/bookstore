import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableHeaders {
  public authors: string[] = ['id', 'name', 'printingEditions', 'edit', 'delete'];
  public users: string[] = ['id', 'userName', 'email', 'status', 'edit', 'delete'];

}
