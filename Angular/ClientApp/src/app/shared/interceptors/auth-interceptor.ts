import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { catchError, tap, switchMap } from "rxjs/operators";
import { Router } from "@angular/router";
import { AccountService } from "../services/account.service";
import { BaseModel } from "../models/base/base.model";
import { RouterConstants } from "../constants/router-constants";
import { UserFilterModel } from "../models/filter/user.filter.model";
import { UsersModel } from "../models/user/users.model";

import { mergeMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private cookieService: CookieService, private router: Router, private accountService: AccountService,
        private route: RouterConstants, private httpClient: HttpClient) { }
    isRefreshed: boolean = false

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        debugger
        let newRequest = this.makeAuthRequest(request)
        return next.handle(newRequest).pipe(catchError(
            (error) => {
                debugger
                if (error.status == 403) {
                    this.router.navigate([this.route.login]);
                    return
                };
                if (error.status == 401) {
                    let accessToken = this.cookieService.get("AccessToken");
                    let refreshToken = this.cookieService.get("RefreshToken");
                    if (refreshToken.length == 0 || accessToken.length == 0) {
                        this.router.navigate([this.route.login]);
                        return
                    }
                    /* this.accountService.refresh(this.cookieService.get("RefreshToken")).pipe(switchMap(jwt => {
                        return next.handle(this.makeAuthRequest(request)); 
                    }))*/
                    this.accountService.refresh(this.cookieService.get("RefreshToken")).subscribe((response: BaseModel) => {
                        debugger
                        if (response.errors.length != 0) {
                            this.router.navigate([this.route.login]);
                        }
                        if (response.errors.length == 0) {
                             next.handle(this.makeAuthRequest(request));
                        }
                    })

                }
                return next.handle(this.makeAuthRequest(request));
                }
        ))}
      

    /* return next.handle(newRequest).pipe(catchError(
         (error) => {
             debugger
             if (error.status == 403) {
                 this.router.navigate([this.route.login]);
                 return
             };
             if (error.status == 401) {
                 let accessToken = this.cookieService.get("AccessToken");
                 let refreshToken = this.cookieService.get("RefreshToken");
                 if (refreshToken.length == 0 || accessToken.length == 0) {
                     this.router.navigate([this.route.login]);
                     return
                 }
                 this.accountService.refresh(refreshToken).subscribe((response: BaseModel) => {
                     debugger
                     if (response.errors.length != 0) {
                         this.router.navigate([this.route.login]);
                     }
                     if (response.errors.length == 0) {
                          next.handle(this.makeAuthRequest(request));
                     }
                 })
 
             };
            
     ))*/

    /* return next.handle(newRequest).pipe(tap(
        success => { },
        error => {
            debugger
            if (error.status == 403) {
                this.router.navigate([this.route.login]);
            };
            if (error.status == 401) {
                let accessToken = this.cookieService.get("AccessToken");
                let refreshToken = this.cookieService.get("RefreshToken");
                if (refreshToken.length == 0 || accessToken.length == 0) {
                    this.router.navigate([this.route.login]);
                    return
                }
                this.accountService.refresh(refreshToken).subscribe((response: any) => {
                    debugger
                    if (!response.errors) {
                        debugger
                        let newAccessToken = this.cookieService.get("AccessToken");
                        const newOneRequest = request.clone({
                            headers: request.headers.set('Authorization', "Bearer " + newAccessToken)
                        });
                       return next.handle(newRequest);
                       debugger
                    }
                    if (response.errors.length == 0) {
                        return next.handle(this.makeAuthRequest(request));
                    }
                })
            }
        }))
 
}*/


    makeAuthRequest(request: HttpRequest<any>): HttpRequest<any> {
        let accessToken = this.cookieService.get("AccessToken");
        const newRequest = request.clone({
            headers: request.headers.set('Authorization', "Bearer " + accessToken)
        });
        return newRequest
    }

    refresh(request: HttpRequest<any>, next: HttpHandler) {
        this.accountService.refresh(this.cookieService.get("RefreshToken")).pipe(switchMap(jwt => {
            return next.handle(this.makeAuthRequest(request));
        }))

    }
}
