export enum SortingOption
{
    None = 0,
    Asc = 1,
    Desc = 2
}