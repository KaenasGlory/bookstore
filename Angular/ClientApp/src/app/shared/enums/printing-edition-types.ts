export enum PrintingEditionTypes {
    Book = 0,
    Newspaper = 1,
    Magazine = 2
}