export enum SortingColumn
{
    Id = 0,
    Price = 1,
    Date = 2,
    TotalPrice = 3
}