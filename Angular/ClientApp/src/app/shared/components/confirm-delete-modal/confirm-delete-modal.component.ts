import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AuthorsComponent } from 'src/app/admin/authors/authors.component';

@Component({
  selector: 'app-confirm-delete-modal',
  templateUrl: './confirm-delete-modal.component.html',
  styleUrls: ['./confirm-delete-modal.component.css']
})
export class ConfirmDeletingModalComponent implements OnInit{
 
  @Inject(MAT_DIALOG_DATA) public data: { deletable: string }
  public dialogRef: MatDialogRef<AuthorsComponent>
  isConfirmed: boolean

  constructor() {  }
  
   ngOnInit(): void {
    debugger
    let a = this.data.deletable
    debugger
    this.isConfirmed = false
  }
  confirm() {
    this.isConfirmed = true
    this.cancel();
  }
  cancel() {
    
    this.dialogRef.close(this.isConfirmed);
  }
}
