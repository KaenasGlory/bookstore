import { Component } from '@angular/core';
import { RouterConstants } from '../../constants/router-constants';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded: boolean
  constructor(private route: RouterConstants) { 
    this.isExpanded = false;
  }
  
 
  collapse():void {
    this.isExpanded = false;
  }

  toggle():void{
    this.isExpanded = !this.isExpanded;
  }
}
