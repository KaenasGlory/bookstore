import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { RouterConstants } from "../constants";
import { PrintingEditionModel } from "../models/printing-edition/printing-edition.model";
import { Observable } from "rxjs";

@Injectable()
export class PrintingEditionService {

    constructor(private httpClient: HttpClient, private route: RouterConstants){
        
    }
    getPrintingEdition(id:number):Observable<PrintingEditionModel>{
      let stringId = id.toString();
        let headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
          });
          let options = { headers: headers,params: new HttpParams()
            .set('id', stringId), withCredentials: true };
          return this.httpClient.get<PrintingEditionModel>(this.route.serverGetPrintingEdition, options);
    }
}
