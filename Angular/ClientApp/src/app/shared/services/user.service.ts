import { Injectable } from "@angular/core";
import { UserChangePasswordModel } from "../models/user/user-change-password.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RouterConstants } from "../constants";
import { Observable } from "rxjs";
import { BaseModel, UserModel } from "../models";

@Injectable()
export class UserService {
  constructor(private httpClient: HttpClient, private route: RouterConstants) { }

  edit(user: UserModel): Observable<BaseModel> {
    debugger
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, withCredentials: true };
    return this.httpClient.post<BaseModel>(this.route.serverEditUser, user, options);
  }

  changePassword(userChangePasswordModel:UserChangePasswordModel):Observable<BaseModel>{
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, withCredentials: true };
    return this.httpClient.post<BaseModel>(this.route.serverChangePassword, userChangePasswordModel, options);
  }
}