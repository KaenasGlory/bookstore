import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material";
import { CommonConstants } from "../constants/common-constants";
import { BaseModel } from "../models/base/base.model";

@Injectable()
export class ErrorService {

    constructor(private constant: CommonConstants, private snackBar: MatSnackBar) { }

    private openError(error: string) {
        debugger
        let config = new MatSnackBarConfig();
        config.panelClass = ['custom-class'];
        this.snackBar.open(error, null, { duration: this.constant.errorShowingDuration })
    }

    checkError(model? : any) : boolean{
        debugger
        if(model==null){
            this.openError(this.constant.nullModel)
            return false
        }
        if (model.errors.length != 0){
            this.openError(model.errors.toString())
            return false
        }
        return true
    }
}