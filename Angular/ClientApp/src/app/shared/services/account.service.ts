import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user/user.model';

import { RegistrationModel } from '../models/auth/registration.model';
import { BaseModel } from '../models/base/base.model';
import { CookieService } from 'ngx-cookie-service';
import { RouterConstants } from '../constants/router-constants';
import { LogInModel } from '../models/auth/log-in.model';
@Injectable()
export class AccountService {

  constructor(private httpClient: HttpClient, private route: RouterConstants, private cookiesService: CookieService) {
  }

  logIn(model: LogInModel): Observable<UserModel> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, withCredentials: true };
    return this.httpClient.post<UserModel>(this.route.serverLogin, model, options);
  }

  registrate(model: RegistrationModel):Observable<BaseModel> {
    debugger
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, withCredentials: true };
    return this.httpClient.post<BaseModel>(this.route.serverRegister, model, options)
  }

  forgotPassword(email: string):Observable<BaseModel> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = {
      headers: headers, params: new HttpParams()
        .set('email', email)
    };
    return this.httpClient.post<BaseModel>(this.route.serverForgotPassword, {}, options)
  }

  confirmAccount(userEmail: string, confirmToken: string): Observable<boolean> {
    /* let params =new HttpParams().set('userEmail',email).set('confirmToken',token);*/
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, withCredentials: true, params: new HttpParams().set('userEmail', userEmail).set('confirmToken', confirmToken) };
    return this.httpClient.post<boolean>(this.route.serverConfirmAccount, {}, options)
  }

  refresh( refreshToken:string):Observable<BaseModel>{
    debugger
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = {
      headers: headers, params: new HttpParams()
        .set('refreshToken', refreshToken),withCredentials: true
    };
    return this.httpClient.post<BaseModel>(this.route.serverRefreshTokens, {}, options)
  }
}