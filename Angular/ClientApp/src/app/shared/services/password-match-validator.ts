import { ValidatorFn, FormGroup } from "@angular/forms";

export const PasswordMatchValidator: ValidatorFn = (formGroup: FormGroup) => {
    const password = formGroup.get('password').value;
    const confirmPassword = formGroup.get('confirmPassword').value;
    if (confirmPassword !== password || password === null || confirmPassword === null) { return { 'mismatch': true } }
  
    return null
  }