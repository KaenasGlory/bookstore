import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthorFilterModel } from '../models/filter/author.filter.model';
import { AuthorModel } from '../models/author/author.model';
import { AuthorsWithPrintingEditionsModel } from '../models/author/authors-with-printing-editions.model';
import { RouterConstants } from '../constants/router-constants';
import { UserFilterModel } from '../models/filter/user.filter.model';
import { UsersModel } from '../models/user/users.model';
@Injectable()
export class AdminService {

  constructor(private httpClient: HttpClient, private route: RouterConstants) {
  }

  createAuthor(author: AuthorModel): Observable<any> {
    return this.httpClient.post(this.route.serverCreateAuthor, author);
  }

  getAllAuthors(filter: AuthorFilterModel): Observable<AuthorsWithPrintingEditionsModel> {
    return this.httpClient.post<AuthorsWithPrintingEditionsModel>(this.route.serverGetAllAuthors, filter);
  }

  deleteAuthor(id: number){
    debugger
    let idString = id.toString(10);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, params:new HttpParams()
      .set('id', idString)};
    return this.httpClient.get(this.route.serverDeleteAuthor, options);
  }

  editAuthor(author: AuthorModel){
    return this.httpClient.post(this.route.serverEditAuthor, author);
  }

  getAllUsers(filter: UserFilterModel): Observable<UsersModel> {
    return this.httpClient.post<UsersModel>(this.route.serverGetAllUsers, filter);
  }

  deleteUser(id: number){
    let idString = id.toString(10);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, params:new HttpParams()
      .set('id', idString)};
    return this.httpClient.get(this.route.serverDeleteUser, options);
  }

  blockUser(id: number){
    let idString = id.toString(10);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let options = { headers: headers, params:new HttpParams()
      .set('id', idString)};
    return this.httpClient.get(this.route.serverBlockUser, options);
  }
}