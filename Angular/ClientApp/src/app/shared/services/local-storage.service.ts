import { CommonConstants } from "../constants";
import { UserModel } from "../models";
import { Injectable } from "@angular/core";
@Injectable()
export class LocalStorageService {
    constructor(private commonConstants: CommonConstants){ }
    private setItem(key: string, value: string): void {
        if (value == null) {
            value = this.commonConstants.emptyString
        }
        localStorage.setItem(key, value);
    }

    setUser( userModel:UserModel):void{
        var user: { [key: string] : string } = {
            "id":userModel.id.toString(),
            "firstName":userModel.firstName,
            "lastName":userModel.lastName,
            "email":userModel.email
        };

        for (let key in user){
            this.setItem(key, user[key])
        }
    }
}