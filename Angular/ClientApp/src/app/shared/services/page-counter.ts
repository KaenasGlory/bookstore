export class PageCounter {
    
    count(itemsCount: number, pageSize: number): number {
        return Math.ceil(itemsCount / pageSize)
    }
}