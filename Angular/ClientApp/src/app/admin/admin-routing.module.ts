import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorsComponent } from './authors/authors.component';
import { UsersComponent } from './users/users.component';

export const routes: Routes = [
  {path :'authors', component : AuthorsComponent},
  {path :'users', component : UsersComponent},
];

@NgModule({
  exports: [RouterModule]
})
export class AdminRoutingModule { }
