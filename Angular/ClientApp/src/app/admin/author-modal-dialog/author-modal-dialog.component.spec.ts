import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorModalDialogComponent } from './author-modal-dialog.component';

describe('AuthorModalComponent', () => {
  let component: AuthorModalDialogComponent;
  let fixture: ComponentFixture<AuthorModalDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorModalDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorModalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
