import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogContent } from '@angular/material';
import { AuthorsComponent } from '../authors/authors.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService, ErrorService } from 'src/app/shared/services/index';
import { AuthorModel, BaseModel } from 'src/app/shared/models/index';
import { ValidationPatterns } from 'src/app/shared/constants/validation-patterns';

@Component({
  selector: 'app-author-modal-dialog',
  templateUrl: './author-modal-dialog.component.html',
  styleUrls: ['./author-modal-dialog.component.css']
})
export class AuthorModalDialogComponent implements OnInit {
  authorValidations: FormGroup;
  authorModel: AuthorModel  
  error: BaseModel
  isItemAddedOrModified: boolean

  constructor(
    public dialogRef: MatDialogRef<AuthorsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { author: AuthorModel, isChanged: boolean },
    private formBuilder: FormBuilder,
    private pattern: ValidationPatterns,
    private adminService: AdminService,
    private errorService: ErrorService) { 
      this.authorModel = this.data.author;
      this.isItemAddedOrModified = false;
    }
    
  ngOnInit(): void {
    debugger
    this.authorValidations = this.formBuilder.group({
      firstName: [this.data.author.firstName, [Validators.required, Validators.pattern(this.pattern.name)]],
      lastName: [this.data.author.lastName, [Validators.pattern(this.pattern.name)]]
    });
  }
  cancelClick(): void {
    debugger
    this.dialogRef.close(this.isItemAddedOrModified);
  }

  addClick(): void {
    this.authorModel.firstName = this.authorValidations.value.firstName;
    this.authorModel.lastName = this.authorValidations.value.lastName;
    this.adminService.createAuthor(this.authorModel).subscribe((response: BaseModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        this.isItemAddedOrModified = true;
        this.cancelClick();
      }
    })
  }

  editClick(): void {
    debugger
    if (this.authorValidations.value.firstName == this.data.author.firstName &&
      this.authorValidations.value.lastName == this.authorModel.lastName) {
      return this.cancelClick();
    }
    this.data.author.firstName = this.authorValidations.value.firstName;
    this.data.author.lastName = this.authorValidations.value.lastName;
    this.adminService.editAuthor(this.data.author).subscribe((response: BaseModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        this.isItemAddedOrModified = true;
        this.cancelClick();
      }
    })
  }
}
