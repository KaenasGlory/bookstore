import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule, routes } from './admin-routing.module';
import { AuthorModalDialogComponent } from './author-modal-dialog/author-modal-dialog.component';
import { AuthorsComponent } from './authors/authors.component';
import { UserModalDialogComponent } from './user-modal-dialog/user-modal-dialog.component';
import { UsersComponent } from './users/users.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AuthorModalDialogComponent, AuthorsComponent, UserModalDialogComponent, UsersComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    RouterModule.forChild(routes),
    MaterialModule,
    SharedModule
  ],
  entryComponents:[
    AuthorModalDialogComponent,
    UserModalDialogComponent
  ]
})
export class AdminModule { }
