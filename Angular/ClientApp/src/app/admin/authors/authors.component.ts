import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, PageEvent, MatSnackBar, Sort } from '@angular/material';
import { CommonConstants } from 'src/app/shared/constants/common-constants';
import { PageCounter } from 'src/app/shared/services/page-counter';
import { AuthorFilterModel } from 'src/app/shared/models/filter/author.filter.model';
import { AdminService } from 'src/app/shared/services/admin.service';
import { AuthorModalDialogComponent } from '../author-modal-dialog/author-modal-dialog.component';
import { AuthorsWithPrintingEditionsModel } from 'src/app/shared/models/author/authors-with-printing-editions.model';
import { AuthorWithPrintingEditionsModel } from 'src/app/shared/models/author/author-with-printing-editions.model';
import { AuthorModel } from 'src/app/shared/models/author/author.model';
import { SortingColumn } from 'src/app/shared/enums/sorting-column';
import { SortingOption } from 'src/app/shared/enums/sorting-option';
import { ErrorService } from 'src/app/shared/services/error.service';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { TableHeaders } from 'src/app/shared/constants/table-headers';
import { first } from 'rxjs/operators';
import { ConfirmDeletingModalComponent } from 'src/app/shared/components/confirm-delete-modal/confirm-delete-modal.component';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  // authors: AuthorsWithPrintingEditionsModel }
  filterModel: AuthorFilterModel = new AuthorFilterModel();
  tableHeader: string[] = ['id', 'name', 'printingEditions', 'edit', 'delete'];
  error: string;
  authors: AuthorModel[]
  isItemAdded: boolean;
  isItemEdited: boolean = false;
  length: number;
  pageSize: number;
  pageEvent: PageEvent;
  headers: string[];

  constructor(/*  private pageCounter:PageCounter, */public dialog: MatDialog,
    private adminService: AdminService,
    private errorService: ErrorService,
    private tableHeaders: TableHeaders,
    public commonConstant: CommonConstants,
  ) {
    debugger
    this.filterModel.pageSize = this.commonConstant.pageSize
    this.filterModel.numberOfPage = this.commonConstant.firstPage
    this.pageSize = this.commonConstant.pageSize;
    this.headers = ["id", "firstName", "lastName", "edit", "delete"]
  }

  ngOnInit(): void {
    this.getAll()
  }

  getPage(event: PageEvent): void {
    if (event != null) {
      this.filterModel.pageSize = event.pageSize;
      this.filterModel.numberOfPage = event.pageIndex + 1;
    }
    this.getAll();
  }

  sort(sort: Sort): void {
    this.filterModel.sortingColumn = SortingColumn.Id;
    if (sort != null && sort.active && (sort.direction === 'asc' || sort.direction === '')) {
      this.filterModel.sortingOption = SortingOption.Asc
    }
    if (sort != null && sort.active && sort.direction === 'desc') {
      this.filterModel.sortingOption = SortingOption.Desc
    }
    this.getAll()
  }

  getAll(): void {
    debugger
    this.adminService.getAllAuthors(this.filterModel).subscribe((response: any) => {
      debugger
      this.authors = response.items
      this.length = response.itemsCount
    })
  }

  openDialog(authorModel?: AuthorModel): void {
    debugger
    let author: AuthorModel = { id: 0, firstName: this.commonConstant.emptyString, lastName: this.commonConstant.emptyString };
    if (authorModel != undefined) {
      author = authorModel;
    }
    const dialogRef = this.dialog.open(AuthorModalDialogComponent, { data: { author: author} });
    debugger
    dialogRef.afterClosed().subscribe((result: boolean) => {
      debugger
      if (result) {
        this.getAll();
      }
    })
  }

  confirmDelete(authorWithPrintingEditions: AuthorWithPrintingEditionsModel): void {
    debugger
    var deletable = this.commonConstant.emptyString
    const deleteConfirmation = this.dialog.open(ConfirmDeletingModalComponent, {
      data: { deletable: deletable}
    })
    deleteConfirmation.componentInstance.data.deletable = "Author "
    let IsConfirmed = deleteConfirmation.afterClosed().subscribe((response: boolean) => {
      if (response) {
        this.delete(authorWithPrintingEditions.author.id)
      }
    })
  }

  delete(id: number) {
    this.adminService.deleteAuthor(id).subscribe((response: BaseModel) => {
      debugger

      if (response.errors.length != 0) {
        this.errorService.checkError(response)
      }
      if (response.errors.length == 0) {
        this.getAll();
      }
    })
  }
}