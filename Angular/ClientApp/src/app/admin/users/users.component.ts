import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, PageEvent, Sort, MatSlideToggleChange, MatSelectionListChange, MatSelectChange } from '@angular/material';
import { AdminService } from 'src/app/shared/services/admin.service';
import { CommonConstants } from 'src/app/shared/constants/common-constants';
import { ErrorService } from 'src/app/shared/services/error.service';
import { UserModel } from 'src/app/shared/models/user/user.model';
import { UserFilterModel } from 'src/app/shared/models/filter/user.filter.model';
import { TableHeaders } from 'src/app/shared/constants/table-headers';
import { SortingColumn } from 'src/app/shared/enums/sorting-column';
import { SortingOption } from 'src/app/shared/enums/sorting-option';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { UserModalDialogComponent } from '../user-modal-dialog/user-modal-dialog.component';
import { UsersModel } from 'src/app/shared/models/user/users.model';
import { UserStatus } from 'src/app/shared/enums/user-status';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: UserModel[]
  filterModel: UserFilterModel
  tableHeader: string[]
  isItemAdded: boolean;
  isItemEdited: boolean = false;
  length: number;
  pageSize = this.commonConstant.pageSize;
  pageEvent: PageEvent;
  userStatus: typeof UserStatus = UserStatus;
  statusSelectionList: string[]
  selectedStatuses: string[]

  constructor(public dialog: MatDialog,
    private adminService: AdminService,
    private errorService: ErrorService,
    private tableHeaders: TableHeaders,
    public commonConstant: CommonConstants) {
    this.filterModel = { 
      numberOfPage: commonConstant.firstPage, 
      userStatus: UserStatus.All, 
      pageSize: commonConstant.pageSize 
    };

    this.tableHeader = tableHeaders.users
    this.statusSelectionList = [UserStatus[UserStatus.Active], UserStatus[UserStatus.Blocked]]
  }

  ngOnInit() {
    this.getAll()
    debugger
  }

  getPage(event: PageEvent): void {
    if (event) {
      this.filterModel.pageSize = event.pageSize;
      this.filterModel.numberOfPage = event.pageIndex + 1;
    }

    this.getAll();
  }

  sort(sort: Sort): void {
    this.filterModel.sortingColumn = SortingColumn.Id;
    if (sort != null && sort.active && (sort.direction === (SortingOption.Asc).toString() || sort.direction === (SortingOption.None).toString())) {
      this.filterModel.sortingOption = SortingOption.Asc
    }

    if (sort != null && sort.active && sort.direction === (SortingOption.Desc).toString()) {
      this.filterModel.sortingOption = SortingOption.Desc
    }

    this.getAll()
  }

  selectStatus(event: MatSelectChange): void {

    if (this.selectedStatuses.length == this.statusSelectionList.length || this.selectedStatuses.length == 0) {
      this.filterModel.userStatus = UserStatus.All
    }

    if (this.selectedStatuses.length < this.statusSelectionList.length) {
      this.filterModel.userStatus = UserStatus[this.selectedStatuses.toString()]
    }

    this.filterModel.numberOfPage = this.commonConstant.firstPage
    this.getAll()
  }

  getAll(): void {
    this.adminService.getAllUsers(this.filterModel).subscribe((response: UsersModel) => {
      debugger
      this.users = response.items
      this.length = response.count
    })
  }

  openDialog(user?: UserModel): void {

    //let author : AuthorModel =  {id : 0, firstName : "", lastName:""};
    /* if (user != undefined) {
      user = authorWithPrintingEditions.author;
    } */
    const dialogRef = this.dialog.open(UserModalDialogComponent, { data: { user: user } });
    // dialogRef.componentInstance.isItemAdded = false;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getAll();
      }
    })
  }

  delete(user: UserModel): void {
    this.adminService.deleteUser(user.id).subscribe((response: BaseModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        this.getAll();
      }
    })
  }

  block(event: MatSlideToggleChange, user: UserModel): void {

    this.adminService.blockUser(user.id).subscribe((response: BaseModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        this.getAll();
      }
    })
  }
}
