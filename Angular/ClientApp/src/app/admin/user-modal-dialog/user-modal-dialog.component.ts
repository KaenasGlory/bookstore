import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersComponent } from '../users/users.component';
import { UserModel } from 'src/app/shared/models/user/user.model';
import { ValidationPatterns } from 'src/app/shared/constants/validation-patterns';
import { AdminService } from 'src/app/shared/services/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ErrorService } from 'src/app/shared/services/error.service';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { ThrowStmt } from '@angular/compiler';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-user-modal-dialog',
  templateUrl: './user-modal-dialog.component.html',
  styleUrls: ['./user-modal-dialog.component.css']
})
export class UserModalDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UsersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {user:UserModel, isChanged : boolean},
    private formBuilder: FormBuilder,
    private pattern: ValidationPatterns,
    private adminService: AdminService,
    private errorService: ErrorService,
    private userService: UserService,
    ) { }

  userValidations: FormGroup;
  userModel: UserModel = this.data.user;
  isItemAdded: boolean = false
  isItemEdited: boolean = false

  ngOnInit() {
    debugger
    this.userValidations = this.formBuilder.group({
      firstName: [this.data.user.firstName, [ Validators.pattern(this.pattern.name)]],
      lastName: [this.data.user.lastName, [Validators.pattern(this.pattern.name)]],
      email: [{value:this.data.user.email, disabled:true}]
    });
  }
  cancelClick() {
    this.dialogRef.close(this.isItemAdded);
  }

  editClick() {
    debugger
    if (this.userValidations.value.firstName == this.data.user.firstName &&
      this.userValidations.value.lastName == this.userModel.lastName) {
      return this.cancelClick();
    }
    this.data.user.firstName = this.userValidations.value.firstName;
    this.data.user.lastName = this.userValidations.value.lastName;
    this.userService.edit(this.data.user).subscribe((response: BaseModel) => {
      let isSuccessful = this.errorService.checkError(response)
      if (isSuccessful) {
        debugger
        this.isItemEdited = true;
        this.cancelClick();
      }
      }
    )
  }
}
