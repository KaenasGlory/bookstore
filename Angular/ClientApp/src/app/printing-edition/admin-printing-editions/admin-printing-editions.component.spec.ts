import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPrintingEditionsComponent } from './admin-printing-editions.component';

describe('AdminPrintingEditionsComponent', () => {
  let component: AdminPrintingEditionsComponent;
  let fixture: ComponentFixture<AdminPrintingEditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPrintingEditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPrintingEditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
