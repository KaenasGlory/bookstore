import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPrintingEditionsComponent as UserPrintingEditionsComponent } from './user-printing-editions.component';

describe('PrintingEditionsComponent', () => {
  let component: UserPrintingEditionsComponent;
  let fixture: ComponentFixture<UserPrintingEditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPrintingEditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPrintingEditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
