import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PrintingEditionService, ErrorService } from 'src/app/shared/services/index';
import { BaseModel, PrintingEditionModel } from 'src/app/shared/models/index';
import { CommonConstants } from 'src/app/shared/constants';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-printing-edition',
  templateUrl: './printing-edition.component.html',
  styleUrls: ['./printing-edition.component.css']
})
export class PrintingEditionComponent implements OnInit {
  amount: number
  printingEditionModel: PrintingEditionModel
  constructor(private formBuilder: FormBuilder,
    private printingEditionService: PrintingEditionService,
    private errorService: ErrorService,
    private commonConstants: CommonConstants,
    private activatedRoute: ActivatedRoute) {
    this.amount = this.commonConstants.defoultAmount;
    this.printingEditionModel = new PrintingEditionModel();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      debugger;
      let id = params["id"];
      this.getPrintingEditionById(id);
    })
  }

  getPrintingEditionById(id: number): void {
    debugger
    this.printingEditionService.getPrintingEdition(id).subscribe((response: PrintingEditionModel) => {
      let result = this.errorService.checkError(response);
      if (result) {
        this.printingEditionModel = response;
      }
    })
  }
}
