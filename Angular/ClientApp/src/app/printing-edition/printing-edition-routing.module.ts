import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPrintingEditionsComponent } from './user-printing-editions/user-printing-editions.component';
import { AdminPrintingEditionsComponent } from './admin-printing-editions/admin-printing-editions.component';
import { PrintingEditionComponent } from './printing-edition/printing-edition.component';

const routes: Routes = [
  {path: 'catalog', component: UserPrintingEditionsComponent},
  {path: 'printing-editions', component: AdminPrintingEditionsComponent},
  {path: 'item', component: PrintingEditionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrintingEditionRoutingModule { }
