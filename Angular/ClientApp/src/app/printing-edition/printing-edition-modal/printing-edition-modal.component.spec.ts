import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintingEditionModalComponent } from './printing-edition-modal.component';

describe('PrintingEditionModalComponent', () => {
  let component: PrintingEditionModalComponent;
  let fixture: ComponentFixture<PrintingEditionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintingEditionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintingEditionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
