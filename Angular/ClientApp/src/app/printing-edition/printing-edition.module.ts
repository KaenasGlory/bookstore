import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrintingEditionRoutingModule } from './printing-edition-routing.module';
import { UserPrintingEditionsComponent } from './user-printing-editions/user-printing-editions.component';
import { PrintingEditionComponent } from './printing-edition/printing-edition.component';
import { AdminPrintingEditionsComponent } from './admin-printing-editions/admin-printing-editions.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [UserPrintingEditionsComponent, PrintingEditionComponent, AdminPrintingEditionsComponent],
  imports: [
    CommonModule,
    PrintingEditionRoutingModule,
    SharedModule
  ]
})
export class PrintingEditionModule { }
