export * from './register/register.component'
export * from './confirm-account/confirm-account.component'
export * from './login/login.component'
export * from './forgot-password/forgot-password.component'
