import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmAccountComponent } from './confirm-account/confirm-account.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

export const routes: Routes = [
  {path :'login', component : LoginComponent},
  {path:'register', component: RegisterComponent},
  {path:'confirm-account', component: ConfirmAccountComponent},
  {path:'forgot-password', component: ForgotPasswordComponent}
];

@NgModule({
  exports:[RouterModule]
})
export class AccountRoutingModule { }
