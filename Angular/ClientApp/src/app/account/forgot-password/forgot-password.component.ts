import { Component } from '@angular/core';
import { AccountService, ErrorService} from 'src/app/shared/services/index';
import { BaseModel } from 'src/app/shared/models/base/base.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonConstants, ValidationPatterns} from 'src/app/shared/constants/index';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  isSuccessful: boolean
  baseModel: BaseModel
  emailValidations: FormGroup

  constructor(private accountService: AccountService,
    private router: Router,
    private formBuilder: FormBuilder,
    private commonConstants: CommonConstants,
    private pattern: ValidationPatterns,
    private errorService: ErrorService) {
    this.isSuccessful = false;
    this.baseModel = new BaseModel();
    this.emailValidations = this.formBuilder.group({
      email: [this.commonConstants.emptyString, [Validators.required, Validators.pattern(this.pattern.email)]]
    })
  }

  forgotPassword(): void {
    this.accountService.forgotPassword(this.emailValidations.value.email).subscribe((responce: BaseModel) => {
      debugger
      this.isSuccessful = this.errorService.checkError(responce)
    })
  }

  toLoginPage():void {
    this.router.navigate(['/account/login'])
  }
}
