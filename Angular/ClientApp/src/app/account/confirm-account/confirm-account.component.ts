import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AccountService } from 'src/app/shared/services/index';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css']
})
export class ConfirmAccountComponent {

  greetingsName: string;
  isConfirmed: boolean;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private accountService: AccountService) {
    this.isConfirmed = false
    this.activatedRoute.queryParams.subscribe((params:Params) => {
      debugger;
      this.isConfirmed = params["isConfirmed"];
      this.greetingsName = params["greetingsName"];
    })
  }
  continue() : void{
    this.router.navigate(['/account/login']);
  }
}

