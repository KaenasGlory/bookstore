import { Component } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AccountService, ErrorService, LocalStorageService } from 'src/app/shared/services/index';
import { RouterConstants } from 'src/app/shared/constants/router-constants';
import { ValidationPatterns, CommonConstants } from 'src/app/shared/constants/index';
import { MatSnackBar } from '@angular/material';
import { LogInModel, UserModel, BaseModel } from 'src/app/shared/models/index';
import { Router } from '@angular/router';
import { UserModule } from 'src/app/user/user.module';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: UserModel
  loginModel: LogInModel
  isRemembered: boolean
  error: any;
  loginValidations: FormGroup;
  snackBar: MatSnackBar;
  isPasswordHidden: boolean = true;

  constructor(private formBuilder: FormBuilder,
    private accountService: AccountService,
    private pattern: ValidationPatterns,
    private route: RouterConstants,
    private commonConstants: CommonConstants,
    private errorService: ErrorService,
    private router: Router,
    private localStorageService:LocalStorageService) {

    this.user = new UserModel();
    this.loginModel = new LogInModel();
    this.isRemembered = true;
    this.loginValidations = this.formBuilder.group({
      email: [this.commonConstants.emptyString, [Validators.required, Validators.pattern(this.pattern.email)]],
      password: [this.commonConstants.emptyString, [Validators.required, Validators.minLength(this.commonConstants.passwordLength), Validators.pattern(this.pattern.password)]]
    });
  }

  login(): void {
    debugger
    this.accountService.logIn(this.loginValidations.value).subscribe((response: UserModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        //this.localStorageService.setUser(response);
        this.router.navigate(['/user/profile'])
      };
    })
  }
}
