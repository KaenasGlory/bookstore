import { Component } from '@angular/core';
import { RegistrationModel, BaseModel } from 'src/app/shared/models/index';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl, ValidatorFn, FormGroupDirective, NgForm } from '@angular/forms';
import { CommonConstants } from 'src/app/shared/constants/common-constants';
import { ValidationPatterns } from 'src/app/shared/constants/validation-patterns';
import { PasswordMatchValidator, CrossFieldErrorMatcher, ErrorService, AccountService } from 'src/app/shared/services/index';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registrationForm: FormGroup;
  registrationModel: RegistrationModel
  baseModel: BaseModel
  isPasswordHidden: boolean
  isConfirmPasswordHidden: boolean
  crossFieldMatcher: CrossFieldErrorMatcher

  constructor(private accountService: AccountService,
    private router: Router,
    private formBuilder: FormBuilder,
    private pattern: ValidationPatterns,
    private commonConstants: CommonConstants,
    private errorService: ErrorService, ) {
    this.registrationModel = new RegistrationModel()
    this.baseModel = new BaseModel()
    this.isPasswordHidden = true
    this.isConfirmPasswordHidden = true
    this.crossFieldMatcher = new CrossFieldErrorMatcher()
    this.registrationForm = this.formBuilder.group({
      firstName: new FormControl(this.commonConstants.emptyString, [Validators.maxLength(20), Validators.pattern(this.pattern.name)]),
      lastName: new FormControl(this.commonConstants.emptyString, [Validators.maxLength(20), Validators.pattern(this.pattern.name)]),
      email: new FormControl(this.commonConstants.emptyString, [Validators.required, Validators.pattern(this.pattern.email)]),
      passwordGroup: this.formBuilder.group({
        password: new FormControl(this.commonConstants.emptyString, [Validators.required, Validators.minLength(this.commonConstants.passwordLength), Validators.maxLength(20), Validators.pattern(this.pattern.password)]),
        confirmPassword: new FormControl(this.commonConstants.emptyString, [Validators.required])
      },
        { validator: PasswordMatchValidator }
      )
    });
  }

  register(): void {
    debugger
    let model: RegistrationModel = {
      firstName: this.registrationForm.value.firstName,
      lastName: this.registrationForm.value.lastName,
      email: this.registrationForm.value.email,
      password: this.registrationForm.value.passwordGroup.password,
    }
    // проверить что отправляется
    this.accountService.registrate(this.registrationForm.value).subscribe((response: BaseModel) => {
      let isSucessful = this.errorService.checkError(response)
      if (isSucessful) {
        this.router.navigate(['/account/confirm-account'])
      }
    });
  }
}





