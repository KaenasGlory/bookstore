import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routes } from './account-routing.module';
import { RouterModule } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ConfirmAccountComponent, ForgotPasswordComponent, RegisterComponent, LoginComponent} from './index';
import { MaterialModule } from '../material/material.module';
import { RouterConstants } from '../shared/constants/router-constants';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ConfirmAccountComponent,
    ForgotPasswordComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    SharedModule
  ],
  providers: [
    CookieService, 
    RouterConstants
  ],
  exports: [
    RouterModule, 
    MaterialModule
  ]
})
export class AccountModule { }
