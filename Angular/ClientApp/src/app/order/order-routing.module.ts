import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserOrderComponent } from './user-order/user-order.component';
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [
  { path: 'my-orders', component: UserOrderComponent },
  { path: 'orders', component: OrdersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
