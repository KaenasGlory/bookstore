import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { UserOrderComponent } from './user-order/user-order.component';
import { OrdersComponent } from './orders/orders.component';

@NgModule({
  declarations: [UserOrderComponent, OrdersComponent],
  imports: [
    CommonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
