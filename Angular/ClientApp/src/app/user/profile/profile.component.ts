import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { BaseModel, UserModel } from 'src/app/shared/models';
import { CrossFieldErrorMatcher, ErrorService, PasswordMatchValidator, LocalStorageService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import { ValidationPatterns, CommonConstants } from 'src/app/shared/constants';
import { UserChangePasswordModel } from 'src/app/shared/models/user/user-change-password.model';
import { UserService } from 'src/app/shared/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { ChangePasswordModalDialogComponent } from '../change-password-modal-dialog/change-password-modal-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit{

  userProfileForm: FormGroup;
  userModel: UserModel
  baseModel: BaseModel
  isPasswordHidden: boolean
  isConfirmPasswordHidden: boolean
  crossFieldMatcher: CrossFieldErrorMatcher
  isEditing: boolean

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private pattern: ValidationPatterns,
    private commonConstants: CommonConstants,
    private errorService: ErrorService,
    private userService: UserService,
    private localStorageService: LocalStorageService,
    public dialog: MatDialog) {
    this.isEditing = false
    this.baseModel = new BaseModel()
    this.isPasswordHidden = true
    this.isConfirmPasswordHidden = true
    this.crossFieldMatcher = new CrossFieldErrorMatcher();
    this.userModel = new UserModel();
  }

ngOnInit(){
this.userProfileForm = this.formBuilder.group({
      firstName: new FormControl({value: localStorage.getItem("firstName"), disabled: true}, [Validators.maxLength(20), Validators.pattern(this.pattern.name)]),
      lastName: new FormControl({ value: localStorage.getItem("lastName"), disabled: true }, [Validators.maxLength(20), Validators.pattern(this.pattern.name)]),
      email: new FormControl({ value: localStorage.getItem("email"), disabled: true }, [Validators.required, Validators.pattern(this.pattern.email)]),
    });
}
  edit() {
    debugger
    this.userModel.id = Number(localStorage.getItem('id'));
    this.userModel.firstName = this.userProfileForm.get('firstName').value;
    this.userModel.lastName = this.userProfileForm.get('lastName').value;
    this.userModel.email = this.userProfileForm.get('email').value
    this.userService.edit(this.userModel).subscribe((responce: BaseModel) => {
      let result = this.errorService.checkError(responce)
      if(result){
        this.localStorageService.setUser(this.userModel);
      }
      this.ngOnInit();
    })
  }

  openChangePasswordDialiog() {
    let id = Number(localStorage.getItem('id'));
    const dialogRef = this.dialog.open(ChangePasswordModalDialogComponent, { data: { id: id } });
    debugger
    dialogRef.afterClosed().subscribe()
  }
  
  turnOnEditionMode(){
      this.userProfileForm.controls.firstName.enable();
      this.userProfileForm.controls.lastName.enable();
      this.userProfileForm.controls.email.enable();
  } 

  cancel(){
    this.isEditing = false;
    this.ngOnInit();
  }
}
