import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonConstants, ValidationPatterns } from 'src/app/shared/constants';
import { PasswordMatchValidator, ErrorService } from 'src/app/shared/services';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProfileComponent } from '../profile/profile.component';
import { UserService } from 'src/app/shared/services/user.service';
import { UserModel, BaseModel } from 'src/app/shared/models';
import { UserChangePasswordModel } from 'src/app/shared/models/user/user-change-password.model';

@Component({
  selector: 'app-change-password-modal-dialog',
  templateUrl: './change-password-modal-dialog.component.html',
  styleUrls: ['./change-password-modal-dialog.component.css']
})
export class ChangePasswordModalDialogComponent implements OnInit {

  passwordeForm: FormGroup
  isCurrentPasswordHidden: boolean
  isNewPasswordHidden: boolean
  isConfirmPasswordHidden: boolean

  constructor(private formBuilder: FormBuilder,
    private commonConstants: CommonConstants,
    private pattern: ValidationPatterns,
    private errorService: ErrorService,
    private userService: UserService,
    public dialogRef: MatDialogRef<ProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number },
  ) {
    this.passwordeForm = this.formBuilder.group({
      currentPassword: new FormControl(this.commonConstants.emptyString, [Validators.required, Validators.minLength(this.commonConstants.passwordLength), Validators.maxLength(20), Validators.pattern(this.pattern.password)]),
      password: new FormControl(this.commonConstants.emptyString, [Validators.required, Validators.minLength(this.commonConstants.passwordLength), Validators.maxLength(20), Validators.pattern(this.pattern.password)]),
      confirmPassword: new FormControl(this.commonConstants.emptyString, [Validators.required])
    },
      { validator: PasswordMatchValidator }
    );
    this.isConfirmPasswordHidden = true
    this.isCurrentPasswordHidden = true
    this.isNewPasswordHidden = true
  }

  ngOnInit() {
  }
  changePassword(): void {
    let userChangePasswordModel: UserChangePasswordModel = {
      id: this.data.id,
      currentPassword: this.passwordeForm.get('currentPassword').value,
      newPassword: this.passwordeForm.get('password').value
    };
    this.userService.changePassword(userChangePasswordModel).subscribe((response: BaseModel) => {
      debugger
      this.errorService.checkError(response);
      this.close();
    });
  }

  close(): void {
    this.dialogRef.close();
  }
}
