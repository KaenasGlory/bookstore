import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';
import { ChangePasswordModalDialogComponent } from './change-password-modal-dialog/change-password-modal-dialog.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ChangePasswordModalDialogComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents:[
    ChangePasswordModalDialogComponent
  ]
})
export class UserModule { }
