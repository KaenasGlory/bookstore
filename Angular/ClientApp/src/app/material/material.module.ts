import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatDividerModule,
  MatCheckboxModule,
  MatIconModule,
  MatPaginatorModule,
  MatTableModule,
  MatHeaderCell,
  MatHeaderCellDef,
  MatDialogModule,
  MatSnackBarModule,
  MatSortModule,
  MatTooltipModule,
  MatSlideToggleModule,
  MatOptionModule,
  MatSelectModule
} from '@angular/material';

@NgModule({
  imports: [

    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    MatDividerModule,
    MatCheckboxModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSortModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatOptionModule,
    MatSelectModule
  ],

  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    MatDividerModule,
    MatCheckboxModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSortModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatOptionModule,
    MatSelectModule,
    MatDividerModule
  ],

})
export class MaterialModule { }
