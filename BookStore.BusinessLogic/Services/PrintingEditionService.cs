﻿using BookStore.BusinessLogicLayer.Extensions;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Repositories;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Common.Constants.Constants;
using static BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper;

namespace BookStore.BusinessLogicLayer.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        private readonly IPrintingEditionRepository _printingEditionRepository;
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IAuthorInPrintingEditionService _authorInPrintingEditionService;


        public PrintingEditionService(IPrintingEditionRepository printingEditionRepository, IAuthorInPrintingEditionRepository authorInPrintingEditionRepository, IAuthorRepository authorRepository, IAuthorInPrintingEditionService authorInPrintingEditionService)
        {
            _printingEditionRepository = printingEditionRepository;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _authorInPrintingEditionService = authorInPrintingEditionService;
        }

        public async Task<BaseModel> CreateAsync(PrintingEditionModelItem printingEditionModelItem)
        {
            var baseModel = new BaseModel();
            if (printingEditionModelItem == null)
            {
                baseModel.Errors.Add(Errors.DataNotArrived);
                return baseModel;
            }
            if (printingEditionModelItem.Currency != Enums.Currency.USD)
            {
                printingEditionModelItem.Price = CurrencyConverterExtension.Convert(printingEditionModelItem.Price, printingEditionModelItem.Currency, Enums.Currency.USD);
                printingEditionModelItem.Currency = Enums.Currency.USD;
            }
            var printingEdition = PrintingEditionMapper.MapToEntity(printingEditionModelItem);
            var printingEditionId = await _printingEditionRepository.CreateAsync(printingEdition);
            if (printingEditionId == 0)
            {
                baseModel.Errors.Add(Errors.OperationFailed);
                return baseModel;
            }
            var authorIds = AuthorMapper.MapToIdList(printingEditionModelItem.Authors);
            var result = await _authorInPrintingEditionService.CreateAuthorInPrintingEditionAsync(printingEditionModelItem.Id, authorIds);
            baseModel.Errors.AddRange(result.Errors);
            return baseModel;
        }

        public async Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModelItem)
        {
            var baseModel = new BaseModel();
            if (printingEditionModelItem == null)
            {
                baseModel.Errors.Add(Errors.DataNotArrived);
                return baseModel;
            }
            var printingEdition = await _printingEditionRepository.GetByIdAsync(printingEditionModelItem.Id);
            if (printingEdition == null)
            {
                baseModel.Errors.Add(Errors.PrintingEdtionNotFound);
                return baseModel;
            }
            if (printingEditionModelItem.Currency != Enums.Currency.USD)
            {
                printingEditionModelItem.Price = CurrencyConverterExtension.Convert(printingEditionModelItem.Price, printingEditionModelItem.Currency, Enums.Currency.USD);
                printingEditionModelItem.Currency = Enums.Currency.USD;
            }
            printingEdition = PrintingEditionMapper.MapToEntity(printingEditionModelItem, printingEdition);
            var printingEditionResult = await _printingEditionRepository.UpdateAsync(printingEdition);
            if (!printingEditionResult)
            {
                baseModel.Errors.Add(Errors.OperationFailed);
                return baseModel;
            }
            var authorIds = AuthorMapper.MapToIdList(printingEditionModelItem.Authors);
            var authorsResult = await _authorInPrintingEditionRepository.ReplaceAllAuthorsInPrintingEdition(printingEdition.Id, authorIds);
            if (!authorsResult)
            {
                baseModel.Errors.Add(Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<BaseModel> DeleteByIdAsync(long id)
        {
            var baseModel = new BaseModel();
            if (id == 0)
            {
                baseModel.Errors.Add(Errors.DataNotArrived);
                return baseModel;
            }
            var printingEdition = await _printingEditionRepository.GetByIdAsync(id);
            if (printingEdition == null)
            {
                baseModel.Errors.Add(Errors.PrintingEdtionNotFound);
                return baseModel;
            }
            var result = await _printingEditionRepository.DeleteByIdAsync(id);
            if (!result)
            {
                baseModel.Errors.Add(Errors.OperationFailed);
                return baseModel;
            }
            var authorInprintingEditionIds = await _authorInPrintingEditionRepository.GetAuthorInPrintingEditionIdsByPrintingEditionIdAsync(id);
            if (authorInprintingEditionIds.Count == 0)
            {
                baseModel.Errors.Add(Errors.OperationFailed);
                return baseModel;
            }
            baseModel = await _authorInPrintingEditionService.DeleteAuthorInPrintingEditionAsync(authorInprintingEditionIds);
            return baseModel;
        }

        public async Task<PrintingEditionModel> GetAllForAsync(PrintingEditionFilterModel filterModel)
        {
            var mappedPrintingEditionFilterModel = FilterMapper.Map(filterModel);
            var result = await _authorInPrintingEditionRepository.GetAllPrintingEditionsAsync(mappedPrintingEditionFilterModel);
            var mappedResult = PrintingEditionMapper.MapToPresentationModel(result);
            if (mappedResult.ItemsCount == 0 || filterModel.Currency == Enums.Currency.USD)
            {
                return mappedResult;
            }
            foreach (var item in mappedResult.Items)
            {
                item.Price = CurrencyConverterExtension.Convert(item.Price, Enums.Currency.USD, filterModel.Currency);
            }
            return mappedResult;
        }

        public async Task<PrintingEditionModelItem> GetPrintingEditionById(long id)
        {
            var printingEditionModelItem = new PrintingEditionModelItem();
            if (id == 0)
            {
                printingEditionModelItem.Errors.Add(Errors.DataNotArrived);
                return printingEditionModelItem;
            }
            var result = await _authorInPrintingEditionRepository.GetPrintingEditionModelByIdAsync(id);
            if (result == null)
            {
                printingEditionModelItem.Errors.Add(Errors.PrintingEdtionNotFound);
                return printingEditionModelItem;
            }
            var mappedResult = PrintingEditionMapper.MapToPresentationModel(result);
            return mappedResult;
        }
    }
}

