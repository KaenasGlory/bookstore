﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.DataAccessLayer.Repositories;
using System.Linq;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper;

namespace BookStore.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<BaseModel> DeleteByIdAsync(long id)
        {
            var baseModel = new BaseModel();
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }
            user.IsRemoved = true;
            var result = await _userRepository.UpdateAsync(user);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<BaseModel> EditUserProfileAsync(UserModelItem userModel)
        {
            var baseModel = new BaseModel();
            if (userModel == null)
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var user = await _userRepository.GetByIdAsync(userModel.Id);
            if (user == null)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }
            user = UserMapper.MapToEntity(userModel, user);
            var changeProfileResult = await _userRepository.UpdateAsync(user);
            if (!changeProfileResult)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
            }
            return baseModel;
        }

        public async Task<BaseModel> BlockUserByIdAsync(long id)
        {
            var baseModel = new BaseModel();
            var user = await _userRepository.GetByIdAsync(id);
            if (user == null)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }
            var result = await _userRepository.BlockByIdAsync(id);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<UserModel> GetAllAsync(UserFilterModel searchModel)
        {
            var mappedSearchModel = FilterMapper.Map(searchModel);
            var result = await _userRepository.GetAllAsync(mappedSearchModel);
            var mappedResult = UserMapper.MapEntityListToModel(result);
            return (mappedResult);
        }

        public async Task<BaseModel> ChangePasswordAsync(UserChangePasswordModel userChangePasswordModel)
        {
            var baseModel = new BaseModel();
            if (userChangePasswordModel == null)
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var user = await _userRepository.GetByIdAsync(userChangePasswordModel.Id);
            if (user == null)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }
            var changePasswordResult = await _userRepository.ChangePasswordAsync(user, userChangePasswordModel.CurrentPassword, userChangePasswordModel.NewPassword);
            if (changePasswordResult.Errors.Any())
            {
                var error = changePasswordResult.Errors.First().Code.ToString();
                baseModel.Errors.Add(error);
            }
            return baseModel;
        }
    }
}

