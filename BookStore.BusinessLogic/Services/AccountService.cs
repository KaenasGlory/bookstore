﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Auth;
using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.DataAccessLayer.Repositories;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailHelper _emailHelper;
        private readonly IPasswordHelper _passwordHelper;

        public AccountService(IUserRepository userRepository, IEmailHelper emailHelper, IPasswordHelper passwordHelper)
        {
            _userRepository = userRepository;
            _emailHelper = emailHelper;
            _passwordHelper = passwordHelper;
        }

        public async Task<string> GetNameByEmailAsync(string email)
        {
            var user = await _userRepository.GetByEmailAsync(email);
            var name = user.Email;
            if (!string.IsNullOrWhiteSpace(user.FirstName))
            {
                name = user.FirstName + " " + user.LastName;
            }
            return name;
        }

        public async Task<string> RegisterAsync(RegistrationModel model)
        {
            var user = UserMapper.MapToEntity(model);
            var confirmationToken = await _userRepository.RegisterAsync(user, model.Password);
            return confirmationToken;
        }

        public async Task<bool> SendConfirmationEmailAsync(string email, string callbackUrl)
        {
            await _emailHelper.SendEmailAsync(email, Constants.EmailContent.ConfirmEmail,
                $"Confirm your account : <a  href={callbackUrl} target='_blank'> {callbackUrl} </a>");
            return true;
        }

        public async Task<BaseModel> ConfirmEmailAsync(string email, string confirmationToken)
        {
            var baseModel = new BaseModel();
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(confirmationToken))
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var result = await _userRepository.ConfirmEmailAsync(email, confirmationToken);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<UserModelItem> LogInAsync(LogInModel logInModel)
        {
            var userModelItem = new UserModelItem();
            if (logInModel == null)
            {
                userModelItem.Errors.Add(Constants.Errors.DataNotArrived);
                return userModelItem;
            }
            var user = await _userRepository.GetByEmailAsync(logInModel.Email);
            if(user == null)
            {
                userModelItem.Errors.Add(Constants.Errors.UserNotFound);
                return userModelItem;
            }
            var result = await _userRepository.LogInAsync(user, logInModel.Password);
            if (result.IsNotAllowed)
            {
                userModelItem.Errors.Add(result.IsNotAllowed.ToString());
                return userModelItem;
            }
            if (result.IsLockedOut)
            {
                userModelItem.Errors.Add(result.IsLockedOut.ToString());
                return userModelItem;
            }
            if (!result.Succeeded)
            {
                userModelItem.Errors.Add(Constants.Errors.OperationFailed);
                return userModelItem;
            }
            userModelItem = UserMapper.MapToModel(user);
            userModelItem.Role = await _userRepository.GetRoleAsync(user);
            
            return userModelItem;
        }

        public async Task LogOutAsync()
        {
            await _userRepository.LogOutAsync();
        }

        public async Task<BaseModel> ForgotPasswordAsync(string email)
        {
            var baseModel = new BaseModel();
            if (string.IsNullOrWhiteSpace(email))
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var user = await _userRepository.GetByEmailAsync(email);
            if (user == null)
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }
            var newPassword = _passwordHelper.GeneratePassword(Constants.PasswordParameters.PasswordLength);
            var result = await _userRepository.ForgotPasswordAsync(user, newPassword);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
                return baseModel;
            }
            await _emailHelper.SendEmailAsync(email, Constants.EmailContent.NewPassword, $"Your new password is : {newPassword}.");
            return baseModel;
        }

        public async Task<UserModelItem> GetUserInRoleByEmailAsync(string email)
        {
            var userModelItem = new UserModelItem();
            var user = await _userRepository.GetByEmailAsync(email);
            if (user == null)
            {
                userModelItem.Errors.Add(Constants.Errors.UserNotFound);
                return userModelItem;
            }
            var role = await _userRepository.GetRoleAsync(user);
            userModelItem.Email = email;
            userModelItem.Role = role;
            return userModelItem;
        }
    }
}
