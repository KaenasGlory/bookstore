﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Auth;
using BookStore.BusinessLogicLayer.Models.Users;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer
{
    public interface IAccountService
    {
        Task<string> GetNameByEmailAsync(string email);
        Task<string> RegisterAsync(RegistrationModel model);
        Task<bool> SendConfirmationEmailAsync(string email, string callbackUrl);
        Task<BaseModel> ConfirmEmailAsync(string email, string confirmationToken);
        Task<UserModelItem> LogInAsync(LogInModel model);
        Task LogOutAsync();
        Task<BaseModel> ForgotPasswordAsync(string userEmail);
        Task<UserModelItem> GetUserInRoleByEmailAsync(string email);
    }
}