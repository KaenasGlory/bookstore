﻿using BookStore.BusinessLogicLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IAuthorInPrintingEditionService
    {
        Task<BaseModel> DeleteAuthorInPrintingEditionAsync(List<long> authorInPrintingEditionIds);
        Task<BaseModel> CreateAuthorInPrintingEditionAsync(long printingEdionId, List<long> authorIds);
        Task<BaseModel> DeleteByAuthorAsync(long authorId);
    }
}
