﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Complex;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Models.Orders;
using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public interface IOrderService
    {
        Task<OrderModel> GetAllAsync(OrderFilterModel orderFilterModel);
        Task<BaseModel> CreateAsync(PaymentIdAndCartModelAndUser paymentIdAndCartModelAndUser);
    }

}
