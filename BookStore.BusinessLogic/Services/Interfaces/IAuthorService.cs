﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Authors;
using BookStore.BusinessLogicLayer.Models.Filters.Base;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public interface IAuthorService
    {
        Task<BaseModel> CreateAsync(AuthorModelItem authorItemModel);
        Task<BaseModel> UpdateAsync(AuthorModelItem authorItemModel);
        Task<BaseModel> DeleteByIdAsync(long authorId);
        Task<AuthorModel> GetAllAsync(BaseFilterModel filterModel);
    }
}
