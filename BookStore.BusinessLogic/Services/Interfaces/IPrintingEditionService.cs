﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Filters;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public interface IPrintingEditionService
    {
        Task<BaseModel> CreateAsync(PrintingEditionModelItem printingEditionModel);
        Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModelItem);
        Task<BaseModel> DeleteByIdAsync(long id);
        Task<PrintingEditionModel> GetAllForAsync(PrintingEditionFilterModel filterModel);
        Task<PrintingEditionModelItem> GetPrintingEditionById(long id);
    }
}
