﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Users;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public interface IUserService
    {
        Task<BaseModel> DeleteByIdAsync(long id);
        Task<BaseModel> EditUserProfileAsync(UserModelItem userModel);
        Task<BaseModel> ChangePasswordAsync(UserChangePasswordModel userChangePasswordModel);
        Task<BaseModel> BlockUserByIdAsync(long id);
        Task<UserModel> GetAllAsync (UserFilterModel searchModel);
    }
}
