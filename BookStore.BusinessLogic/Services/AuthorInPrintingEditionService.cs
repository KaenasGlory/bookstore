﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Common.Constants.Constants;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AuthorInPrintingEditionService : IAuthorInPrintingEditionService
    {
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        private readonly IPrintingEditionRepository _printingEditionRepository;
        public AuthorInPrintingEditionService(IAuthorInPrintingEditionRepository authorInPrintingEditionRepository, IAuthorRepository authorRepository, IPrintingEditionRepository printingEditionRepository)
        {
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _printingEditionRepository = printingEditionRepository;
        }
        public async Task<BaseModel> DeleteAuthorInPrintingEditionAsync(List<long> authorInPrintingEditionIds)
        {
            var baseModel = new BaseModel();
            foreach (var id in authorInPrintingEditionIds)
            {
                var result = await _authorInPrintingEditionRepository.DeleteByIdAsync(id);
                if (!result && !baseModel.Errors.Any())
                {
                    baseModel.Errors.Add(Errors.CantDeleteAuthorForThisBook);
                }
            }
            return baseModel;
        }

        public async Task<BaseModel> CreateAuthorInPrintingEditionAsync(long printingEdionId, List<long> authorIds)
        {
            var baseModel = new BaseModel();
            var authorInPrintingEditions = BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper.AuthorInPrintingEditionMapper.MapToModelList(printingEdionId, authorIds);
            foreach (var authorInPrintingEdition in authorInPrintingEditions)
            {
                var result = await _authorInPrintingEditionRepository.CreateAsync(authorInPrintingEdition);
                if (result == 0)
                {
                    baseModel.Errors.Add(Errors.CantSaveAuthorForThisBook + $": {authorInPrintingEdition.Author.FirstName} {authorInPrintingEdition.Author.LastName}");
                }
            }
            return baseModel;
        }

        public async Task<BaseModel> DeleteByAuthorAsync(long authorId)
        {
            var baseModel = new BaseModel();
            var authorInPrintingEditions = await _authorInPrintingEditionRepository.GetAuthorInPrintingEditionsByAuthorIdAsync(authorId);
            if (authorInPrintingEditions.Count() == 0)
            {
                return baseModel;
            }
            var authorInPrintingEditionIds = authorInPrintingEditions.Select(x => x.Id).ToList();
            var printingEditionIds = authorInPrintingEditions.Select(x => x.PrintingEditionId).ToList();
            var deletablePrintingEditionIds = await _authorInPrintingEditionRepository.GetPrintingEditionsWithoutAuthorAsync(printingEditionIds);
            var result = await DeleteAuthorInPrintingEditionAsync(authorInPrintingEditionIds);
            foreach (var printingEditionId in deletablePrintingEditionIds)
            {
                var printingEditionResult = await _printingEditionRepository.DeleteByIdAsync(printingEditionId);
                if (!printingEditionResult && !baseModel.Errors.Any())
                {
                    baseModel.Errors.Add(Errors.CantDeleteBookOfThisAuthor);
                }
            }
            return baseModel;
        }
    }
}
