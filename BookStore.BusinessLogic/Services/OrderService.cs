﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Cart;
using BookStore.BusinessLogicLayer.Models.Complex;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Models.Orders;
using BookStore.DataAccessLayer.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper;

namespace BookStore.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IUserRepository _userRepository;

        public OrderService(IOrderItemRepository orderItemRepository, IPaymentRepository paymentRepository, IOrderRepository orderRepository, IUserRepository userRepository)
        {
            _orderItemRepository = orderItemRepository;
            _orderRepository = orderRepository;
            _paymentRepository = paymentRepository;
            _userRepository = userRepository;
        }
        private async Task<BaseModel> CreateOrderItem(long orderId, List<CartModelItem> cartModelitem)
        {
            var baseModel = new BaseModel();
            foreach (var item in cartModelitem)
            {
                var orderItem = OrderMapper.MapToEntityItem(orderId, item);
                var result = await _orderItemRepository.CreateAsync(orderItem);
                if (result == 0)
                {
                    baseModel.Errors.Add(Constants.Errors.OperationFailed + $": {item} was not added in Orders");
                    continue;
                }
            }
            return baseModel;
        }

        public async Task<OrderModel> GetAllAsync(OrderFilterModel orderFilterModel)
        {
            var mappedFilterModel = FilterMapper.Map(orderFilterModel);
            var result = await _orderItemRepository.GetAllAsync(mappedFilterModel);
            var mappedResult = OrderMapper.MapToPresentationModel(result);
            return mappedResult;
        }

        public async Task<BaseModel> CreateAsync(PaymentIdAndCartModelAndUser paymentIdAndCartModelAndUser)
        {
            var baseModel = new BaseModel();
            if (paymentIdAndCartModelAndUser == null)
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var user = await _userRepository.GetByEmailAsync(paymentIdAndCartModelAndUser.UserEmail);
            var order = OrderMapper.MapToEntity(paymentIdAndCartModelAndUser.CartModel, user);
            var payment = PaymentMapper.MapToEntity(paymentIdAndCartModelAndUser.Payment);
            var paymentId = await _paymentRepository.CreateAsync(payment);
            order.Status = paymentId == 0 ? (DataAccessLayer.Entities.Enums.Enums.OrderStatus)Enums.OrderStatus.Unpaid : (DataAccessLayer.Entities.Enums.Enums.OrderStatus)Enums.OrderStatus.Paid;
            var orderId = await _orderRepository.CreateAsync(order);
            if (orderId == 0)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
                return baseModel;
            }
            var result = await CreateOrderItem(orderId, paymentIdAndCartModelAndUser.CartModel.Items);
            return result;
        }
    }
}
