﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Authors;
using BookStore.BusinessLogicLayer.Models.Filters.Base;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Repositories;
using System.Linq;
using System.Threading.Tasks;
using static BookStore.BusinessLogicLayer.Helpers.Mapper.Mapper;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IAuthorInPrintingEditionRepository _authorInPrintingEditionRepository;
        private readonly IAuthorInPrintingEditionService _authorInPrintingEditionService;

        public AuthorService(IAuthorRepository authorRepository, IAuthorInPrintingEditionRepository authorInPrintingEditionRepository, IAuthorInPrintingEditionService authorInPrintingEditionService)
        {
            _authorRepository = authorRepository;
            _authorInPrintingEditionRepository = authorInPrintingEditionRepository;
            _authorInPrintingEditionService = authorInPrintingEditionService;
        }

        public async Task<BaseModel> CreateAsync(AuthorModelItem author)
        {
            var baseModel = new BaseModel();
            if (author == null || string.IsNullOrWhiteSpace(author.FirstName))
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var mappedAuthor = AuthorMapper.MapToEntity(author);
            var result = await _authorRepository.CreateAsync(mappedAuthor);
            if (result == 0)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<BaseModel> UpdateAsync(AuthorModelItem authorModel)
        {
            var baseModel = new BaseModel();
            if (authorModel == null)
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var author = await _authorRepository.GetByIdAsync(authorModel.Id);
            if (author == null)
            {
                baseModel.Errors.Add(Constants.Errors.AuthorNotFound);
                return baseModel;
            }
            author.FirstName = authorModel.FirstName;
            author.LastName = authorModel.LastName;
            var result = await _authorRepository.UpdateAsync(author);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
            }
            return baseModel;
        }

        public async Task<BaseModel> DeleteByIdAsync(long id)
        {
            var baseModel = new BaseModel();
            if (id == 0)
            {
                baseModel.Errors.Add(Constants.Errors.DataNotArrived);
                return baseModel;
            }
            var foundedAuthor = await _authorRepository.GetByIdAsync(id);
            if (foundedAuthor == null)
            {
                baseModel.Errors.Add(Constants.Errors.AuthorNotFound);
                return baseModel;
            }
            var result = await _authorRepository.DeleteByIdAsync(id);
            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.OperationFailed);
                return baseModel;
            }
            var authorInPRintingEdition = await _authorInPrintingEditionRepository.GetAuthorInPrintingEditionsByAuthorIdAsync(id);
            if(authorInPRintingEdition.Count == 0)
            {
                return baseModel;
            }
            var authorInPrintingEditionResult = await _authorInPrintingEditionService.DeleteByAuthorAsync(id);
            if (authorInPrintingEditionResult.Errors.Any())
            {
                baseModel.Errors.AddRange(authorInPrintingEditionResult.Errors);
            }
            return baseModel;
        }

        public async Task<AuthorModel> GetAllAsync(BaseFilterModel filterModel)
        {
            var mappedFilterModel = FilterMapper.Map(filterModel);
            var authors = await _authorInPrintingEditionRepository.GetAllAuthorsAsync(mappedFilterModel);
            var mappedAuthors = AuthorMapper.MapToPresentationModel(authors);

            return mappedAuthors;
        }
    }
}
