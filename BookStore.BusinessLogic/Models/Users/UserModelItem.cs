﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;


namespace BookStore.BusinessLogicLayer.Models.Users
{
    public class UserModelItem : BaseModel
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        [Required]
        public string Email { get; set; }
        public long Id { get; set; }
        public string Role { get; set; }
        public Enums.UserStatus Status { get; set; }
        public bool IsRemoved { get; set; }
    }
     
}