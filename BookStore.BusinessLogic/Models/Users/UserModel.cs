﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogicLayer.Models.Users
{
    public class UserModel : BaseModel
    {
        public List<UserModelItem> Items { get; set; } = new List<UserModelItem>();
        public int Count { get; set; }
    }
}
