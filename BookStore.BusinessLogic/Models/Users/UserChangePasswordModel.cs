﻿namespace BookStore.BusinessLogicLayer.Models.Users
{
    public class UserChangePasswordModel
    {
        public long Id { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }

    }
}
