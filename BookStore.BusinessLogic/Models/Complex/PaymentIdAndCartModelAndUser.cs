﻿using BookStore.BusinessLogicLayer.Models.Cart;
using BookStore.BusinessLogicLayer.Models.Payments;

namespace BookStore.BusinessLogicLayer.Models.Complex
{
    public class PaymentIdAndCartModelAndUser
    {
        public PaymentModelItem Payment { get; set; }
        public CartModel CartModel { get; set; } = new CartModel();
        public string UserEmail { get; set; }
    }
}
