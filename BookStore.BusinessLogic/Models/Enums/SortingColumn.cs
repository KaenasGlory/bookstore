﻿namespace BookStore.BusinessLogicLayer.Models
{
    public partial class Enums
    {
        public enum SortingColumn
        {
            Id = 0,
            Price = 1,
            Date = 2,
            TotalPrice = 3
        }
    }
}
