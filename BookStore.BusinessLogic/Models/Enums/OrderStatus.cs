﻿namespace BookStore.BusinessLogicLayer.Models
{
    public partial class Enums
    {
        public enum OrderStatus
        {
            All = 0,
            Unpaid=1,
            Paid = 2
        }
    }
}
