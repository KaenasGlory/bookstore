﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models.Authors
{
    public class AuthorModelItem : BaseModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> PrintingEditions { get; set; } = new List<string>();
    }
}

