﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models.Authors
{
    public class AuthorModelItem
    {
        public AuthorModelItem Author { get; set; } = new AuthorModelItem();
        public List<string> PrintingEditions { get; set; } = new List<string>();
    }
}
