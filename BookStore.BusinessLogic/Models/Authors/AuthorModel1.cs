﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models.Authors
{
    public class AuthorModel
    {
        public List<AuthorModelItem> AuthorWithPrintingEditions { get; set; } = new List<AuthorModelItem>();
        public int Count { get; set; }
    }
}
