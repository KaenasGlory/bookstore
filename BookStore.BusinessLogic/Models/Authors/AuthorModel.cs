﻿using BookStore.BusinessLogicLayer.Models.Authors;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models
{
    public class AuthorModel : BaseModel
    {
        public List<AuthorModelItem> Items { get; set; } = new List<AuthorModelItem>();
        public int ItemsCount { get; set; }
    }
}

