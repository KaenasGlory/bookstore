﻿using System;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models
{
    public class BaseModel
    {
        public List<string> Errors { get; set; } = new List<string>();
    }
}