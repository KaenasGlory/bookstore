﻿using BookStore.BusinessLogicLayer.Common.Constants;

namespace BookStore.BusinessLogicLayer.Models.Filters.Base
{
    public class BaseFilterModel
    {
        public int NumberOfPage { get; set; } = Constants.PaginationParameters.FirstPage;
        public int PageSize { get; set; } = Constants.PaginationParameters.ItemsOnPage;
        public string SearchText { get; set; }
        public Enums.SortingOption SortingOption { get; set; } = Enums.SortingOption.None;
        public Enums.SortingColumn SortingColumn { get; set; }


    }
}
