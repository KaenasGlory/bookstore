﻿using BookStore.BusinessLogicLayer.Models.Filters.Base;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models.Filters
{
    public class PrintingEditionFilterModel : BaseFilterModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public List<Enums.PrintingEditionType> SelectedTypes { get; set; } = new List<Enums.PrintingEditionType>();
        public Enums.Currency Currency { get; set; }
    }
}
