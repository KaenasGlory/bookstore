﻿using BookStore.BusinessLogicLayer.Models.Filters.Base;

namespace BookStore.BusinessLogicLayer.Models.Filters
{
    public class OrderFilterModel : BaseFilterModel
    {
        public Enums.OrderStatus OrderStatus { get; set; }
        public long UserId { get; set; }
    }
}
