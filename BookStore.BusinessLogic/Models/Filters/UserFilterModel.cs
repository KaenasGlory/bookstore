﻿using BookStore.BusinessLogicLayer.Models.Filters.Base;

namespace BookStore.BusinessLogicLayer.Models.Users
{
    public class UserFilterModel : BaseFilterModel
    {
        public Enums.UserStatus UserStatus { get; set; }
    }
}
