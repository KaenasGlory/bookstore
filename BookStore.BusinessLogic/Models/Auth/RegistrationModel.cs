﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BusinessLogicLayer.Models.Auth
{
    public class RegistrationModel
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
