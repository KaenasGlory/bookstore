﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BusinessLogicLayer.Models.Auth
{
    public class LogInModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
