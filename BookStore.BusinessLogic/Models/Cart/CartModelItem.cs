﻿namespace BookStore.BusinessLogicLayer.Models.Cart
{
    public class CartModelItem
    {
        public long PrintingEditionId { set; get; }
        public string PrintingEditionTitle { get; set; }
        public int Amount { get; set; }
        public decimal OrderItemPrice { get; set; }
    }
}
