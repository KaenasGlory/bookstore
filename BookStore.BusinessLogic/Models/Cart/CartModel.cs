﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogicLayer.Models.Cart
{
    public class CartModel
    {
        public List<CartModelItem> Items { get; set; } = new List<CartModelItem>();
        public decimal TotalPrice { get; set; }
    }
}
