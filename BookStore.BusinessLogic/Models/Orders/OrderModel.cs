﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models.Orders
{
    public class OrderModel : BaseModel
    {
        public List<OrderModelItem> Items { get; set; } = new List<OrderModelItem>();
        public int ItemsCount { get; set; }
    }
}

