﻿namespace BookStore.BusinessLogicLayer.Models.Orders
{
    public class OrderItemModelItem 
    {
        public string Title { get; set; }
        public int Amount { get; set; }
        public Enums.PrintingEditionType PrintingEditionType { get; set; }
    }
}
