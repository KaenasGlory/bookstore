﻿using BookStore.BusinessLogicLayer.Models.Orders;
using System;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models
{
    public class OrderModelItem 
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public decimal TotalPrice { get; set; }
        public List<OrderItemModelItem> Items { get; set; } = new List<OrderItemModelItem>();
        public Enums.OrderStatus Status { get; set; }
    }
}