﻿using BookStore.BusinessLogicLayer.Models.PrintingEditions;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogicLayer.Models
{
    public class PrintingEditionModel
    {
        public List<PrintingEditionWithAuthorsModel> Items { get; set; } = new List<PrintingEditionWithAuthorsModel>();
        public int ItemsCount { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}
