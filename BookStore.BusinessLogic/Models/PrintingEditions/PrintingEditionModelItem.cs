﻿using BookStore.BusinessLogicLayer.Models.Authors;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models
{
    public class PrintingEditionModelItem : BaseModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Enums.PrintingEditionType Type { get; set; }
        public decimal Price { get; set; }
        public Enums.Currency Currency { get; set; }
        public List<AuthorModelItem> Authors { get; set; } = new List<AuthorModelItem>();
    }
}
