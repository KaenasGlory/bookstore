﻿using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Models
{
    public class PrintingEditionModel : BaseModel
    {
        public List<PrintingEditionModelItem> Items { get; set; } = new List<PrintingEditionModelItem>();
        public int ItemsCount { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
    }
}