﻿using BookStore.BusinessLogicLayer.Models.Payments;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogicLayer.Models
{
    public class PaymentModel 
    {
        public List<PaymentModelItem> Items { get; set; } = new List<PaymentModelItem>();
    }
}

