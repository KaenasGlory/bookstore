﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogicLayer.Models.Payments
{
    public class PaymentModelItem : BaseModel
    {

        public string TransactionId { get; set; }
    }
}
