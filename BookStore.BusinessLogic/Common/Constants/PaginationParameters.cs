﻿namespace BookStore.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public class PaginationParameters
        {
            public static int FirstPage = 1;
            public static int ItemsOnPage = 12;
        }
    }
}
