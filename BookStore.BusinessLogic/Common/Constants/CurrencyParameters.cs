﻿using BookStore.BusinessLogicLayer.Models;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public class CurrencyParameters
        {
            public static Dictionary<Enums.Currency, decimal> CurrencyValues = new Dictionary<Enums.Currency, decimal>()
            {
                { Enums.Currency.USD, 1m },
                { Enums.Currency.EUR, 0.91m },
                { Enums.Currency.GBP, 1.29m },
                { Enums.Currency.CHF, 2.06m },
                { Enums.Currency.JPY, 0.81m },
                { Enums.Currency.UAH, 24.72m }
            };
        }
    }
}