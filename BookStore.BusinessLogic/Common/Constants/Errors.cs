﻿namespace BookStore.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public class Errors
        {
            public const string DataNotArrived = "Data hasn't arrived. Please try again";
            public const string OperationFailed = "Operation was failed";

            public const string UserNotFound = "User is not found";
            public const string PrintingEdtionNotFound = "Printing edtion is not found";
            public const string AuthorNotFound = "Author is not found";
            public const string PaymentDoesNotExist = "Payment does not exist";

            public const string CantSaveAuthorForThisBook = "Can't save author for this book";
            public const string CantDeleteAuthorForThisBook = "Can't delete some author for this book";
            public const string CantDeleteBookOfThisAuthor = "Can't delete some book for this author";
            public const string HaveToLogin = "To continue you should login";
            public const string WrongToken = "Token is not valid";
        }
    }
}

