﻿namespace BookStore.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public  class EmailContent
        {
            public const string ConfirmEmail = "Confirm Email";
            public const string NewPassword = "Your new password";
        }
    }
}
