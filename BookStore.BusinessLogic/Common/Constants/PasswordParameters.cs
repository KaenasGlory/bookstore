﻿namespace BookStore.BusinessLogicLayer.Common.Constants

{
    public partial class Constants
    {
        public class PasswordParameters
        {
            public const int PasswordLength = 6;
            public const string ValidSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            public const string Numbers = "1234567890";
        }
    }
}
