﻿namespace BookStore.BusinessLogicLayer.Common.Config.EmailOptions
{

    public class EmailOptionsModel
    {
        public string AdministrationName { get; set; }
        public string BookStoreEmailAdress { get; set; }
        public string GMailHost { get; set; }
        public string Password { get; set; }
        public int PortNumber { get; set; }
    }

}

