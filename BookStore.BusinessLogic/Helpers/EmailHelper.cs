﻿using BookStore.BusinessLogicLayer.Common.Config.EmailOptions;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Helpers
{
    public class EmailHelper : IEmailHelper
    {
        private EmailOptionsModel _emailOptionsModel;
        private readonly IConfiguration _configuration;
        public EmailHelper(IConfiguration configuration) 
        {
            _emailOptionsModel = configuration.GetSection("EmailOptions").Get<EmailOptionsModel>();
        }
        public async Task SendEmailAsync(string toAdress, string subject, string messageText)
        {
            using (var smtpClient = new SmtpClient())
            {
                var message = new MailMessage();
                smtpClient.Host = _emailOptionsModel.GMailHost;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                var basicCredential = new NetworkCredential(_emailOptionsModel.BookStoreEmailAdress, _emailOptionsModel.Password);
                smtpClient.Credentials = basicCredential;

                message.From = new MailAddress(_emailOptionsModel.BookStoreEmailAdress);
                message.Subject = subject;

                message.IsBodyHtml = false;
                message.Body = messageText;
                message.To.Add(toAdress);

                await smtpClient.SendMailAsync(message);
            }
        }
    }
}