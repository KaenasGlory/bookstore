﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class AuthorInPrintingEditionMapper
        {
            public static List<AuthorInPrintingEdition> MapToModelList(long printingEditionId, List<long> authorIds)
            {
                var authorInPrintingEditions = new List<AuthorInPrintingEdition>();
                foreach (var id in authorIds)
                {
                    var authorInPrintingEdition = new AuthorInPrintingEdition
                    {
                        PrintingEditionId = printingEditionId,
                        AuthorId = id
                    };
                    authorInPrintingEditions.Add(authorInPrintingEdition);
                }
                return authorInPrintingEditions;
            }
        }
    }
}
