﻿using BookStore.BusinessLogicLayer.Models;
using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public class PrintingEditionMapper
        {
            public static PrintingEdition MapToEntity(Models.PrintingEditionModelItem updatedModel, PrintingEdition printingEdition)
            {
                var entity = MapToEntity(updatedModel);
                entity.Id = printingEdition.Id;
                return entity;
            }

            public static PrintingEdition MapToEntity(Models.PrintingEditionModelItem model)
            {
                var entity = new PrintingEdition
                {
                    Title = model.Title,
                    Description = model.Description,
                    Price = model.Price,
                    Currency = (DataAccessLayer.Entities.Enums.Enums.Currency)model.Currency,
                    PrintingEditionType = (DataAccessLayer.Entities.Enums.Enums.PrintingEditionType)model.Type
                };
                return entity;
            }

            public static PrintingEditionModelItem MapToPresentationModel(DataAccessLayer.Models.PrintingEditionModel dalModel)
            {
                var bllModel = new PrintingEditionModelItem
                {
                    Id = dalModel.Id,
                    Title = dalModel.Title,
                    Description = dalModel.Description,
                    Type = (Enums.PrintingEditionType)dalModel.Type,
                    Price = dalModel.Price
                };
                foreach (var author in dalModel.Authors)
                {
                    var mappedAuthor = AuthorMapper.MaptoPresentationModelItem(author);
                    bllModel.Authors.Add(mappedAuthor);
                }
                return bllModel;
            }

            private static List<PrintingEditionModelItem> MapToModelList(List<DataAccessLayer.Models.PrintingEditionModel> dalList)
            {
                var bllList = new List<PrintingEditionModelItem>();
                foreach (DataAccessLayer.Models.PrintingEditionModel printingEdition in dalList)
                {
                    var mappedPrintingEdition = MapToPresentationModel(printingEdition);
                    bllList.Add(mappedPrintingEdition);
                }
                return bllList;
            }

            public static PrintingEditionModel MapToPresentationModel(DataAccessLayer.Models.PrintingEditionPageModel dalpageModel)
            {
                var bllPageModel = new PrintingEditionModel()
                {
                    ItemsCount = dalpageModel.ItemsCount,
                    MinPrice = dalpageModel.MinPrice,
                    MaxPrice = dalpageModel.MaxPrice,
                    Items = MapToModelList(dalpageModel.Items)
                };
                return bllPageModel;
            }
        }
    }
}

