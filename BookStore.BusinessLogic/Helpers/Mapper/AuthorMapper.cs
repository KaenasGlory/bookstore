﻿
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Authors;
using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class AuthorMapper
        {
            public static Author MapToEntity(AuthorModelItem model)
            {
                var entity = new Author
                {
                    Id = model.Id,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };
                return entity;
            }

            public static List<long> MapToIdList (List<AuthorModelItem> authors)
            {
                var authorIds = new List<long>();
                foreach (var author in authors)
                {
                    authorIds.Add(author.Id);
                }
                return authorIds;
            }

            private static AuthorModelItem MaptoPresentationModelItem(DataAccessLayer.Models.AuthorWithPrintingEditionsModel dalModel)
            {
                var bllModel = new AuthorModelItem
                {
                    Id = dalModel.Id,
                    FirstName = dalModel.FirstName,
                    LastName = dalModel.LastName
                };
                foreach (var title in dalModel.PrintingEditions)
                {
                    bllModel.PrintingEditions.Add(title);
                }
                return bllModel;
            }

            private static List<AuthorModelItem> MapToPresentationList(List<DataAccessLayer.Models.AuthorWithPrintingEditionsModel> dalModel)
            {
                var bllModel = new List<AuthorModelItem>();

                foreach (var item in dalModel)
                {
                    var mappedItem = MaptoPresentationModelItem(item);
                    bllModel.Add(mappedItem);
                }
                return bllModel;
            }

            public static AuthorModelItem MaptoPresentationModelItem(Author author)
            {
                var authorModelItem = new AuthorModelItem
                {
                    Id = author.Id,
                    FirstName = author.FirstName,
                    LastName = author.LastName
                };
                return authorModelItem;
            }

            public static AuthorModel MapToPresentationModel(DataAccessLayer.Models.AuthorPageModel dalModel)
            {
                var bllModel = new AuthorModel()
                {
                    ItemsCount = dalModel.ItemsCount,
                    Items = MapToPresentationList(dalModel.Items)
                };
                return bllModel;
            }
        }
    }
}