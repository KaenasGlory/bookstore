﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Cart;
using BookStore.BusinessLogicLayer.Models.Orders;
using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class OrderMapper
        {
            private static OrderItemModelItem MapToItemModelItem(OrderItem orderItem)
            {
                var bllOrderModel = new OrderItemModelItem
                {
                    Title = orderItem.PrintingEdition.Title,
                    PrintingEditionType = (Enums.PrintingEditionType)orderItem.PrintingEdition.PrintingEditionType,
                    Amount = orderItem.Amount
                };
                return bllOrderModel;
            }
            private static OrderModelItem MapToModelItem(DataAccessLayer.Models.OrderModel dalOrderModel)
            {
                var bllOrderModel = new OrderModelItem
                {
                    Id = dalOrderModel.Id,
                    Date = dalOrderModel.CreationDate,
                    UserName = dalOrderModel.UserName,
                    Email = dalOrderModel.Email
                };
                foreach (var item in dalOrderModel.Items)
                {
                    var mappedItem = MapToItemModelItem(item);
                    bllOrderModel.Items.Add(mappedItem);
                }
                bllOrderModel.TotalPrice = dalOrderModel.TotalPrice;
                bllOrderModel.Status = (Enums.OrderStatus)dalOrderModel.Status;
                return bllOrderModel;
            }

            public static List<OrderModelItem> MapToModel(List<DataAccessLayer.Models.OrderModel> dalOrderModel)
            {
                var bllOrderModel = new List<OrderModelItem>();
                foreach (var item in dalOrderModel)
                {
                    var mappedItem = MapToModelItem(item);
                    bllOrderModel.Add(mappedItem);
                }
                return bllOrderModel;
            }

            public static OrderModel MapToPresentationModel(DataAccessLayer.Models.OrderPageModel dalModel)
            {
                var orderItem = new OrderModel
                {
                    ItemsCount = dalModel.ItemsCount,
                    Items = MapToModel(dalModel.Items)
                };
                return orderItem;
            }

            public static OrderItem MapToEntityItem(long orderId, CartModelItem cartItem)
            {
                var orderItem = new OrderItem
                {
                    PrintingEditionId = cartItem.PrintingEditionId,
                    Amount = cartItem.Amount,
                    OrderId = orderId
                };
                return orderItem;
            }

            public static Order MapToEntity(CartModel cartModel, User user)
            {
                var order = new Order
                {
                    // order.Id = bllOrderModel.Id;
                    UserId = user.Id,
                    TotalPrice = cartModel.TotalPrice
                };
                return order;
            }
        }
    }
}

