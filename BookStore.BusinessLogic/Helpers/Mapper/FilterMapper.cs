﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Models.Filters.Base;
using BookStore.BusinessLogicLayer.Models.Users;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class FilterMapper
        {
            public static DataAccessLayer.Models.Filters.PrintingEditionFilterModel Map(PrintingEditionFilterModel bllFilterModel)
            {
                var dalFilterModel = new DataAccessLayer.Models.Filters.PrintingEditionFilterModel
                {
                    NumberOfPage = bllFilterModel.NumberOfPage,
                    PageSize = bllFilterModel.PageSize,
                    SearchText = bllFilterModel.SearchText,
                    SortingOption = (DataAccessLayer.Entities.Enums.Enums.SortingOption)bllFilterModel.SortingOption,
                    SortingColumn = (DataAccessLayer.Entities.Enums.Enums.SortingColumn)bllFilterModel.SortingColumn,
                    MinPrice = bllFilterModel.MinPrice,
                    MaxPrice = bllFilterModel.MaxPrice,
                    SelectedPrintingEditionTypes = new List<DataAccessLayer.Entities.Enums.Enums.PrintingEditionType>()
                };
                foreach (Enums.PrintingEditionType printingEditionType in bllFilterModel.SelectedTypes)
                {
                    var mappedPrintingEditionType = (DataAccessLayer.Entities.Enums.Enums.PrintingEditionType)printingEditionType;
                    dalFilterModel.SelectedPrintingEditionTypes.Add(mappedPrintingEditionType);
                };
                return dalFilterModel;
            }

            public static DataAccessLayer.Models.Filters.UserFilterModel Map(UserFilterModel bllFilterModel)
            {
                var dalFilterModel = new DataAccessLayer.Models.Filters.UserFilterModel
                {
                    NumberOfPage = bllFilterModel.NumberOfPage,
                    PageSize = bllFilterModel.PageSize,
                    SearchText = bllFilterModel.SearchText,
                    UserStatus = (DataAccessLayer.Entities.Enums.Enums.UserStatus)bllFilterModel.UserStatus
                };

                return dalFilterModel;
            }

            public static DataAccessLayer.Models.Filters.Base.BaseFilterModel Map(BaseFilterModel bllFilterModel)
            {
                var dalFilterModel = new DataAccessLayer.Models.Filters.Base.BaseFilterModel
                {
                    NumberOfPage = bllFilterModel.NumberOfPage,
                    PageSize = bllFilterModel.PageSize,
                    SortingOption = (DataAccessLayer.Entities.Enums.Enums.SortingOption)bllFilterModel.SortingOption,
                    SortingColumn = (DataAccessLayer.Entities.Enums.Enums.SortingColumn)bllFilterModel.SortingColumn,
                    SearchText = bllFilterModel.SearchText

                };
                return dalFilterModel;
            }

            public static DataAccessLayer.Models.Filters.OrderFilterModel Map(OrderFilterModel bllFilterModel)
            {
                var dalFilterModel = new DataAccessLayer.Models.Filters.OrderFilterModel
                {
                    UserId = bllFilterModel.UserId,
                    NumberOfPage = bllFilterModel.NumberOfPage,
                    PageSize = bllFilterModel.PageSize,
                    SortingOption = (DataAccessLayer.Entities.Enums.Enums.SortingOption)bllFilterModel.SortingOption,
                    OrderStatus = (DataAccessLayer.Entities.Enums.Enums.OrderStatus)bllFilterModel.OrderStatus,
                    SortingColumn = (DataAccessLayer.Entities.Enums.Enums.SortingColumn)bllFilterModel.SortingColumn
                };
                return dalFilterModel;
            }
        }
    }
}
