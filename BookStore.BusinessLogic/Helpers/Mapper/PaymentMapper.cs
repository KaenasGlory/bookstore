﻿using BookStore.BusinessLogicLayer.Models.Payments;
using DataAccessLayer.Entities;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class PaymentMapper
        {
            public static Payment MapToEntity(PaymentModelItem paymentModel)
            {
                var payment = new Payment()
                {
                    TransactionId = paymentModel.TransactionId
                };
                return payment;
            }
        }
    }
}