﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Auth;
using BookStore.BusinessLogicLayer.Models.Users;
using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BookStore.BusinessLogicLayer.Helpers.Mapper
{
    public partial class Mapper
    {
        public static class UserMapper
        {
            public static UserModelItem MapToModel(User entity)
            {
                var model = new UserModelItem
                {
                    Id = entity.Id,
                    FirstName = entity.FirstName,
                    LastName = entity.LastName,
                    Email = entity.Email,
                    Status = Enums.UserStatus.Active
                };
                if (entity.LockoutEnd != null)
                {
                    model.Status = Enums.UserStatus.Blocked;
                }
                return model;
            }

            public static User MapToEntity(UserModelItem editedModel, User currentUser)
            {
                var user = currentUser;
                user.FirstName = editedModel.FirstName;
                user.LastName = editedModel.LastName;
                user.Email = editedModel.Email;
                user.UserName = editedModel.Email;
                return user;
            }
            public static User MapToEntity(RegistrationModel registrationModel)
            {
                var user = new User
                {
                    FirstName = registrationModel.FirstName,
                    LastName = registrationModel.LastName,
                    Email = registrationModel.Email,
                    UserName = registrationModel.Email,
                    IsRemoved = false,
                    LockoutEnabled = true
                };
                return user;
            }

          
            public static UserModel MapEntityListToModel(BookStore.DataAccessLayer.Models.UserModel result)
            {
                var model = new UserModel()
                {
                    Count = result.ItemsCount
                };
                foreach (var user in result.Items)
                {
                    
                    model.Items.Add(MapToModel(user));
                }
                return model;
            }
        }
    }
}
