﻿using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using System;
using System.Security.Cryptography;
using System.Text;
using static BookStore.BusinessLogicLayer.Common.Constants.Constants;
namespace BookStore.BusinessLogicLayer.Helpers
{
    public class PasswordHelper: IPasswordHelper
    {
        public string GeneratePassword(int length)
        {
            var stringBuilder = new StringBuilder();
            using (var randomGenerator = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                for (int i = 0; i < length; i++)
                {
                    randomGenerator.GetBytes(uintBuffer);
                    uint numberOfCharacter = BitConverter.ToUInt32(uintBuffer, 0);
                    stringBuilder.Append(PasswordParameters.ValidSymbols[(int)(numberOfCharacter % (uint)PasswordParameters.ValidSymbols.Length)])
                    ;
                }
                //TODO take random number
                stringBuilder.Append(DateTime.Now.Millisecond);
            }
            return stringBuilder.ToString();
        }
    }
}
