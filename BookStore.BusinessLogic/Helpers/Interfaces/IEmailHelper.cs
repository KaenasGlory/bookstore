﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IEmailHelper
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
