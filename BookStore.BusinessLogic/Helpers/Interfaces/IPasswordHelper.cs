﻿namespace BookStore.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IPasswordHelper
    {
        string GeneratePassword(int length);
    }
}
