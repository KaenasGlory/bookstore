﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Models;

namespace BookStore.BusinessLogicLayer.Extensions
{
    public static class CurrencyConverterExtension
    {
        public static decimal Convert(decimal price, Enums.Currency currencyFrom, Enums.Currency currencyTo)
        {
            var convertedPrice = price * Constants.CurrencyParameters.CurrencyValues[currencyFrom] / Constants.CurrencyParameters.CurrencyValues[currencyTo];
            return convertedPrice;
        }
    }
}
