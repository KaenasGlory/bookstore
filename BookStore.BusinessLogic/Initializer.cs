﻿using BookStore.BusinessLogicLayer.Common.Constants;
using BookStore.BusinessLogicLayer.Helpers;
using BookStore.BusinessLogicLayer.Helpers.Interfaces;
using BookStore.BusinessLogicLayer.Services;
using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.DataAccessLayer.Initialization;
using BookStore.DataAccessLayer.Repositories;
using DataAccessLayer.AppContext;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.BusinessLogicLayer
{
    public static class Initializer
    {
        public static void Init(IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(connectionString));

            services.AddIdentity<User, IdentityRole<long>>(opts =>
            {
                opts.Password.RequiredLength = Constants.PasswordParameters.PasswordLength;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = true;
                opts.Password.RequireUppercase = true;
                opts.Password.RequireDigit = true;
            })
                .AddEntityFrameworkStores<ApplicationContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IAuthorInPrintingEditionRepository, AuthorInPrintingEditionRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IPrintingEditionRepository, PrintingEditionRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IPrintingEditionService, PrintingEditionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthorInPrintingEditionService, AuthorInPrintingEditionService>();

            services.AddTransient<IEmailHelper, EmailHelper>();
            services.AddScoped<IPasswordHelper, PasswordHelper>();

            services.AddTransient<DbInizialization>();
        }
    }
}
