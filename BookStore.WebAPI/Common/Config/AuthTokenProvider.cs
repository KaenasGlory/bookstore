﻿using System;

namespace BookStore.WebAPI.Common.Config
{
    public partial class Config
    {
        public  class AuthTokenProvider
        {
            public const string JwtKey = "agladsfkghasli80еАdhglaisuhgliauwrg";
            public const string JwtIssuer = "MyServer";
            public const string JwtAudience = "https://localhost:44370/";
            public static TimeSpan AccessTokenExpiration { get; set; } = TimeSpan.FromMinutes(10);
            public static TimeSpan RefreshTokenExpiration { get; set; } = TimeSpan.FromDays(30);

        }
    }
}