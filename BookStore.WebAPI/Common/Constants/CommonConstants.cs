﻿namespace BookStore.WebAPI.Common.Constants
{
    public partial class Constants
    {
        public class CommonConstants
        {
            public static string AccountControllerName = "api/account";
            public static string ConfirmRegistrationActionName = "confirmRegistration";
            public static string UserAlreadyExist = "This user has been already exist.Please confirm your account or use another email";
            public static string CantSentEmail = "CantSentEmail";
        }
    }
}
