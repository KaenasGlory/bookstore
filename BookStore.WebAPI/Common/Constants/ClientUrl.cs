﻿namespace BookStore.WebAPI.Common.Constants
{
    public partial class Constants
    {
        public class ClientUrl
        {
            public static string Client = "http://localhost:4200/";
            public static string ConfirmAccount = "account/confirm-account";
        }
    }
}
