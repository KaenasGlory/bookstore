﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using Microsoft.AspNetCore.Diagnostics;

namespace BookStore.WebAPI.Helpers
{
    public class FileLogger : ILogger<ExceptionHandlerMiddleware>
    {
        private readonly string _filePath;
        private readonly object _lock = new object();
       
        public FileLogger(string path)
        {
            _filePath = path;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
        
        public bool IsEnabled(LogLevel logLevel)
        {
            //return logLevel == LogLevel.Trace;
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter == null)
            {
                return;
            }
            lock (_lock)
            {

                File.AppendAllText(_filePath, formatter(state, exception) + Environment.NewLine);
            }
        }



    }
}
