﻿using BookStore.WebAPI.Common.Config;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.IO;

namespace BookStore.WebAPI.Helpers
{
    public class Logger 
    {
        public void Log( ExceptionContext exceptionContext)
        {
            var exception = exceptionContext.Exception;

            string massage = $"{DateTime.Now} ------------------------------------ \n " +
                $"{exception.Message}  in {exceptionContext.ActionDescriptor}\n" 
               /*+ $"Result is {exceptionContext.Result}\n"*/;
            

            if (!File.Exists(Config.LoggerFile.LoggerFilePath))
            {
                File.Create(Config.LoggerFile.LoggerFilePath);
            }

            File.AppendAllText(Config.LoggerFile.LoggerFilePath, massage );
        }
    }
}
