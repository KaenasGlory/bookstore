﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Users;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.WebAPI.Helpers.Interfaces
{
    public interface IJwtHelper
    {
        JwtTokenModel GenerateJwtTokens(UserModelItem user);
        string ValidateToken(string refreshToken);
    }
}
