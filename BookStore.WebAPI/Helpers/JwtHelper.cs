﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.WebAPI.Common.Config;
using BookStore.WebAPI.Helpers.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace BookStore.WebAPI.Helpers
{
    public class JwtHelper : IJwtHelper
    {

        private SecurityToken CreateToken(List<Claim> claims, TimeSpan tokenExpiration)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Config.AuthTokenProvider.JwtKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                        Config.AuthTokenProvider.JwtIssuer,
                        Config.AuthTokenProvider.JwtAudience,
                        claims: claims,
                        expires: DateTime.Now.Add(tokenExpiration),
                        signingCredentials: creds
                        );
            return token;
        }

        public JwtTokenModel GenerateJwtTokens(UserModelItem user)
        {
            List<Claim> refreshClaims = new List<Claim>{
           new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(ClaimTypes.Email, user.Email)};

            List<Claim> accessClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role),
                new Claim(ClaimTypes.Name, user.Email)
            };

            var accessToken = CreateToken(accessClaims, Config.AuthTokenProvider.AccessTokenExpiration);
            var refreshToken = CreateToken(refreshClaims, Config.AuthTokenProvider.RefreshTokenExpiration);

            var encodedAccessToken = new JwtSecurityTokenHandler().WriteToken(accessToken);
            var encodedRefreshToken = new JwtSecurityTokenHandler().WriteToken(refreshToken);
            var jwtTokensModel = new JwtTokenModel(encodedAccessToken, encodedRefreshToken);
            return jwtTokensModel;
        }

        public string ValidateToken(string refreshToken)
        {
            try
            {
                var token = new JwtSecurityTokenHandler().ReadJwtToken(refreshToken);
                var email = token.Claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault().Value;
                if (string.IsNullOrWhiteSpace(email))
                {
                    return null;
                }
                var exp = token.ValidTo;
                if (exp > DateTime.Now)
                {
                    return email;
                }
                return null;
            }
            catch (Exception exception)
            {
                return null;
            }

        }

    }


}

