﻿using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("delete")]
        public async Task<IActionResult> DeleteAsync(long id)
        {
            var baseModel = await _userService.DeleteByIdAsync(id);
            return Ok(baseModel);
        }

        [HttpPost("edit")]
        public async Task<IActionResult> EditProfileByAdminAsync(UserModelItem userProfile)
        {
            var basemodel = await _userService.EditUserProfileAsync(userProfile);
            return Ok(basemodel);
        }

        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePasswordAsync(UserChangePasswordModel userChangePasswordModel)
        {
            var basemodel = await _userService.ChangePasswordAsync(userChangePasswordModel);
            return Ok(basemodel);
        }

        [HttpGet("block")]
        public async Task<IActionResult> BlockUserAsync(long id)
        {
            var basemodel = await _userService.BlockUserByIdAsync(id);
            return Ok(basemodel);
        }

        [HttpPost("getAll")]
        public async Task<IActionResult> GetAllAsync(UserFilterModel searchModel)
        {
            var result = await _userService.GetAllAsync(searchModel);
            return Ok(result);
        }
    }
}