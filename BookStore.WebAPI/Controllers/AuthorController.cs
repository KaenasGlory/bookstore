﻿using BookStore.BusinessLogicLayer.Models.Authors;
using BookStore.BusinessLogicLayer.Models.Filters.Base;
using BookStore.BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]

    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync(AuthorModelItem authorModelItem)
        {
            var baseModel = await _authorService.CreateAsync(authorModelItem);
            return Ok(baseModel);
        }

        [HttpGet("delete")]
        public async Task<IActionResult> DeleteAsync(long id)
        {
            var baseModel = await _authorService.DeleteByIdAsync(id);
            return Ok(baseModel);
        }

        [HttpPost("edit")]
        public async Task<IActionResult> EditAsync(AuthorModelItem authorModelItem)
        {
            var result = await _authorService.UpdateAsync(authorModelItem);
            return Ok(result);
        }

        [HttpPost("getAll")]
        public async Task<IActionResult> GetAllAsync(BaseFilterModel baseFilterModel)
        {
            var result = await _authorService.GetAllAsync(baseFilterModel);
            return Ok(result);
        }
    }
}