﻿using BookStore.BusinessLogicLayer;
using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Auth;
using BookStore.BusinessLogicLayer.Models.Users;
using BookStore.BusinessLogicLayer.Services;
using BookStore.WebAPI.Common.Constants;
using BookStore.WebAPI.Helpers.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;
        private readonly IJwtHelper _jwtHelper;


        public AccountController(IAccountService accountService, IJwtHelper jwtHelper, IUserService userService)
        {
            _accountService = accountService;
            _jwtHelper = jwtHelper;
            _userService = userService;
        }

        private void AppendTokensToCookies(JwtTokenModel tokenModel)
        {
            Response.Cookies.Append("AccessToken", tokenModel.AccessToken, new CookieOptions { IsEssential = true });
            Response.Cookies.Append("RefreshToken", tokenModel.RefreshToken, new CookieOptions { IsEssential = true });
        }

        [HttpPost("registration")]
        public async Task<IActionResult> RegisterAsync(RegistrationModel model)
        {
            var result = new BaseModel();
            var confirmationToken = await _accountService.RegisterAsync(model);
            if (string.IsNullOrEmpty(confirmationToken))
            {
                result.Errors.Add(Constants.CommonConstants.UserAlreadyExist);
                return Ok(result);
            }
            var callbackUrl = Url.Action(
                Constants.CommonConstants.ConfirmRegistrationActionName,
                Constants.CommonConstants.AccountControllerName,
                new { userEmail = model.Email, confirmToken = confirmationToken },
                protocol: HttpContext.Request.Scheme);
            var sendingResult = await _accountService.SendConfirmationEmailAsync(model.Email, callbackUrl);
            if (!sendingResult)
            {
                result.Errors.Add("CantSentEmail");
            }
            return Ok(result);
        }

        [HttpGet("confirmRegistration")]
        public async Task<IActionResult> ConfirmRegistrationAsync(string userEmail, string confirmToken)
        {
            var result = await _accountService.ConfirmEmailAsync(userEmail, confirmToken);
            if (result.Errors.Any())
            {
                return Ok(result);
            }
            var isConfirmed = true;
            var greetingsName = await _accountService.GetNameByEmailAsync(userEmail);
            return Redirect($"{Constants.ClientUrl.Client}{Constants.ClientUrl.ConfirmAccount}?greetingsName={greetingsName}&isConfirmed={isConfirmed}");
            }

        [HttpPost("login"), AllowAnonymous]
        public async Task<IActionResult> LoginAsync(LogInModel model)
        {
            var userModel = await _accountService.LogInAsync(model);
            if (userModel.Errors.Any())
            {
                return Ok(userModel);
            }
            var tokenModel = _jwtHelper.GenerateJwtTokens(userModel);
            if (tokenModel == null)
            {
                userModel.Errors.Add(BookStore.BusinessLogicLayer.Common.Constants.Constants.Errors.OperationFailed);
                return Ok(userModel);
            }
            AppendTokensToCookies(tokenModel);
            return Ok(userModel);
        }

        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> LogOutAsync()
        {
            await _accountService.LogOutAsync();
            return Ok(new BaseModel());
        }

        [HttpPost("forgotPassword")]
        public async Task<IActionResult> ForgotPasswordAsync(string email)
        {
            var baseModel = await _accountService.ForgotPasswordAsync(email);
            return Ok(baseModel);
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> RefreshTokens(string refreshToken)
        {
            var baseModel = new BaseModel();
            var userEmail = _jwtHelper.ValidateToken(refreshToken);
            if (userEmail == null)
            {
                baseModel.Errors.Add(BookStore.BusinessLogicLayer.Common.Constants.Constants.Errors.WrongToken);
                return Ok(baseModel);
            }
            var userModel = await _accountService.GetUserInRoleByEmailAsync(userEmail);
            if (userModel.Errors.Any())
            {
                return Ok(userModel.Errors);
            }
            var tokenModel = _jwtHelper.GenerateJwtTokens(userModel);
            if (tokenModel == null)
            {
                baseModel.Errors.Add(BookStore.BusinessLogicLayer.Common.Constants.Constants.Errors.OperationFailed);
                return Ok(baseModel);
            }
            AppendTokensToCookies(tokenModel);
            return Ok(baseModel);
        }
    }
}