﻿using BookStore.BusinessLogicLayer.Models;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PrintingEditionController : ControllerBase
    {
        private readonly IPrintingEditionService _printingEditionService;
        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync(PrintingEditionModelItem printingEditionModel)
        {
            var baseModel = await _printingEditionService.CreateAsync(printingEditionModel);
            return Ok(baseModel);
        }

        [HttpPost("edit")]
        public async Task<IActionResult> EditAsync(PrintingEditionModelItem printingEditionModelItem)
        {
            var baseModel = await _printingEditionService.UpdateAsync(printingEditionModelItem);
            return Ok(baseModel);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> DeleteAsync(long id)
        {
            var baseModel = await _printingEditionService.DeleteByIdAsync(id);
            return Ok(baseModel);
        }

        [HttpPost("getAll")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllAsync(PrintingEditionFilterModel filterModel)
        {
            var result = await _printingEditionService.GetAllForAsync(filterModel);
            return Ok(result);
        }

        [HttpGet("getPrintingEdition")]
        [AllowAnonymous]
        public async Task<IActionResult> GetPrintingEditionByIdAsync(long id)
        {
            var result = await _printingEditionService.GetPrintingEditionById(id);
            return Ok(result);
        }
    }
}
