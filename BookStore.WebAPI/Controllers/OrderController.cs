﻿using BookStore.BusinessLogicLayer.Models.Complex;
using BookStore.BusinessLogicLayer.Models.Filters;
using BookStore.BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("getAll")]
        public async Task<IActionResult> GetAllAsync(OrderFilterModel searchModel)
        {
            var result = await _orderService.GetAllAsync(searchModel);
            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync(PaymentIdAndCartModelAndUser paymentIdAndCartModelAndUser)
        {
            var baseModel = await _orderService.CreateAsync(paymentIdAndCartModelAndUser);
            return Ok(baseModel);
        }
    }
}