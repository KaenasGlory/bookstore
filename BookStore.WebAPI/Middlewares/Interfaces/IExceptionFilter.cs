﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace BookStore.WebAPI.Middlewares.Interfaces
{
    public interface IExceptionFilter
    {
        void OnException(ExceptionContext filterContext);
    }
}
