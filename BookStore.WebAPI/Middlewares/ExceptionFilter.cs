﻿using System.Threading.Tasks;
using BookStore.WebAPI.Helpers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Filters
{

    public class ExceptionFilter :IExceptionFilter
    {
        private readonly Logger _logger = new Logger();
        public void OnException(ExceptionContext exceptionContext)
        {
            _logger.Log(exceptionContext);
        }
    }
}