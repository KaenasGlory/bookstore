﻿//using BookStore.WebAPI.Extensions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Store.Presentation.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));
            _logger = loggerFactory.CreateLogger("FileLogger");
        }

        public async Task Invoke(HttpContext context)
        {
            
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                _logger.LogError("Log error", exception);
                //try
                //{
                //    _logger.LogError("Log error", exception);
                //    _logger.LogInformation("Log inform", exception);
                //    _logger.LogDebug("Log debug", exception);
                //    _logger.LogCritical("Critical exception", exception);

                //}
                //catch (Exception innerException)
                //{
                //    _logger.LogError(0, innerException, "Error handling exception");
                //}
            }
        }
    }
}